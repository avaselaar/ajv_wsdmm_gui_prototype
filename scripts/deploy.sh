#!/usr/bin/env bash

GUI_DIR=${HOME}/.proto_gui
NIFI_DIR=${HOME}/.nifi

# Setup directories
rm -fr $GUI_DIR
mkdir $GUI_DIR
mkdir -p $GUI_DIR/data/media 

cp ./vti_data.ndjson ${NIFI_DIR}/ndjson_test
ln ${HOME}/WSDMM*.json ${NIFI_DIR}/ndjson_test/wsdmm.conf
sudo chown -R 1000:1000 $GUI_DIR/ 
sudo chmod -R 777 $GUI_DIR/data 

cp env.prod.db.example $GUI_DIR/.env.prod.db
cp env.prod.example $GUI_DIR/.env.prod

# Generate a new secret
NEW_UUID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
sed -i  -e "s/SECRET_KEY.*$/SECRET_KEY=$NEW_UUID/" $GUI_DIR/.env.prod

# Setup docker
cp docker-compose.deploy.yml $GUI_DIR/docker-compose.yml
cd $GUI_DIR
docker-compose up -d

# Wait for containers to come up
sleep 30

# Run some configuration stuff
docker-compose exec web python manage.py migrate --noinput
docker-compose exec web python manage.py collectstatic --no-input --clear
docker-compose exec web python manage.py createsuperuser

docker-compose restart 
