#!/usr/bin/env bash

# Required Files:
# docker-compose.prod.yml
# wsdmm.conf json in ~
# nginx.conf
# env.prod.db example
# env.db example

GUI_DIR=${HOME}/.proto_gui
NIFI_DIR=${HOME}/.nifi
ADMIN_USER="admin"
ADMIN_PASS="admin"

# Setup directories
echo "Setting up data directories..."
rm -fr $GUI_DIR
mkdir -p $GUI_DIR/data/media 
mkdir -p $GUI_DIR/infiles
mkdir -p $GUI_DIR/nginx/conf.d

echo "Copying files..."
sudo chown -R 1000:1000 $GUI_DIR/ 
sudo chmod -R 777 $GUI_DIR/data 
cp ./nginx.conf $GUI_DIR/nginx/conf.d/nginx.conf
sudo chown -R 1000:1000 ${HOME}/WSDMM*.json
ln ${HOME}/WSDMM*.json ${GUI_DIR}/infiles/wsdmm.conf

cp ./env.prod.db.example $GUI_DIR/.env.prod.db
cp ./env.prod.example $GUI_DIR/.env.prod

# Generate a new secret
echo "Generating super secure SECRET KEY!"
NEW_UUID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
sed -i  -e "s/SECRET_KEY.*$/SECRET_KEY=$NEW_UUID/" $GUI_DIR/.env.prod

# Setup docker
echo "Deploying to the swarm..."
docker login
docker stack deploy --compose-file docker-compose.prod.yml wsdmm-gui-stack

# Wait for containers to come up
echo "Waiting for containers to go..."
sleep 30

echo "Configuring the database..."
# Get ID of the GUI container
GUIID=`docker ps -aqf "name=wsdmm_gui\."`

# Run some configuration stuff
docker exec $GUIID python manage.py migrate --noinput
docker exec $GUIID python manage.py collectstatic --no-input --clear
docker exec $GUIID python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('$ADMIN_USER', 'admin@example.com', '$ADMIN_PASS')"
