objects = data/outfiles/* conf

config :
	tar xf VTI_NiFi.tar.xz ; \
	mkdir -p data/outfiles ; \
	mkdir -p data/infiles ; \
	mkdir -p data/logs ; \
	mkdir -p data/plots ; \
	chown -R 1000:1000 * ; \
	chmod -R 777 data ; \
	mv VTI_NiFi/vti_data.ndjson data/infiles ; \
	mv VTI_NiFi/conf .
	rm -fr conf/archive/* ; \
	rm -fr VTI_NiFi

clean : 
	rm -fr $(objects)

prodbuild: 
	docker-compose -f docker-compose.prod.yml up -d --build

produp: 
	docker-compose -f docker-compose.prod.yml up -d 

proddb:
	docker-compose -f docker-compose.prod.yml exec wsdmm_gui python manage.py migrate --noinput
	docker-compose -f docker-compose.prod.yml exec wsdmm_gui python manage.py collectstatic --no-input --clear
	docker-compose -f docker-compose.prod.yml exec wsdmm_gui python manage.py createsuperuser

prodstop: 
	docker-compose -f docker-compose.prod.yml stop 

proddown: 
	docker-compose -f docker-compose.prod.yml down

prodclean: 
	docker-compose -f docker-compose.prod.yml down -v

devbuild:
	docker-compose -f docker-compose.yml up -d --build

devup:
	docker-compose -f docker-compose.yml up -d 

devstop:
	docker-compose -f docker-compose.yml stop

devdown:
	docker-compose -f docker-compose.yml down 

devclean:
	docker-compose -f docker-compose.yml down -v

deploy:
	mkdir deploy ;\
	cp doc/examples/env.prod.db.example deploy/ ;\
	cp doc/examples/env.prod.example deploy/ ;\
	cp doc/examples/wsdmm.conf deploy/ ;\
	cp nginx/nginx.conf deploy/ ;\
	cp docker-compose.prod.yml deploy/ ;\
	cp scripts/deploy_swarm.sh deploy/ ;\
	tar zcvf deploy.tar.gz ./deploy

deployclean:
	rm -fr deploy 
	rm deploy.tar.gz
