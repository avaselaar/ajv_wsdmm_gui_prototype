# Prototype WSDMM_GUI Setup

This repo contains configurations both for a production version and a development version of the site.  The production version uses nginx and gunicorn to serve pages, while the dev version uses the built in python development server.  The setups for them are slightly different, so there are two sets of instructions included.

You'll need the VTI_NiFi.tar.xz from:
https://alstommlb.atlassian.net/wiki/spaces/DACM/pages/301563971/Shunt+Monitoring+App+Deployment+and+Configuration+-+File+version

# Setup - Development Version

1) Clone the git repo:
`git clone git@bitbucket.org:avaselaar/ajv_wsdmm_gui_prototype.git`

2) cd into it
`cd ajv_wsdmm_gui_prototype`

3) Copy VTI_NiFi.tar.xz into ajv_wsdmm_gui_prototype/

4) Setup the files and directories:
`make config`

5) Build the dev version.  This will take a while.
`make devbuild`

6) Add a superuser to django:
`docker-compose -f docker-compose.yml exec web python manage.py createsuperuser`

7) Log in to django
`http://localhost:8000`

8) If you click on `Shunt` at the top right, it should show that processors are disabled.

9) Log into 
`http://localhost:8080/nifi`

10) Enable the File Based Shunt process group

11) Refresh, should show as STOPPED.
`http://localhost:8000/shunt`

12) Go to `Configure`, adjust settings

13) Go back to status, click `Start`

14) Should be working.

# Setup - Production Version

1) Clone the git repo:
`git clone git@bitbucket.org:avaselaar/ajv_wsdmm_gui_prototype.git`

2) cd into it
`cd ajv_wsdmm_gui_prototype`

3) Copy VTI_NiFi.tar.xz into ajv_wsdmm_gui_prototype/

4) Setup the files and directories:
`make config`

5) Copy `env.prod.db.example` and `env.prod.example` to `.env.prod.db` and `.env.prod` respectively, then edit
them with new values for production.  These files are included in gitignore so secret things won't be
commited to git.

6) Build the production version.  The production version uses nginx and gunicorn, while dev uses the built in python test server.  Look at ~Makefile~ to see other available commmands.  This step will take a while, good time for coffee.
`make prodbuild`

7) Setup the database, collect static files for NGINX and create the Django superuser:
`make proddb`

9) Log in to django
`http://localhost`

10) If you click on "Shunt" at the top right, it should show that processors are disabled.

11) Log into 
`http://localhost:8080/nifi`

12) Enable the File Based Shunt process group

13) Refresh, should show as STOPPED.
`http://localhost/shunt`

14) Go to `Configure`, adjust settings

15) Go back to status, click `Start`

16) Should be working, files will show up under `Files`


# Deploy

A third option for deployment is to build the production version above on a
faster machine, such as a WSDMM VM residing on a server, then export the
created docker images to tar files, manually copy them to the target wsdmm and
then load them into docker.

To do this - 

First, on the build machine:
## Build
1) Clone the git repo:
`git clone git@bitbucket.org:avaselaar/ajv_wsdmm_gui_prototype.git`

2) cd into it
`cd ajv_wsdmm_gui_prototype`

3) Copy VTI_NiFi.tar.xz into ajv_wsdmm_gui_prototype/

4) Setup the files and directories:
`make config`

5) Copy `env.prod.db.example` and `env.prod.example` to `.env.prod.db` and `.env.prod` respectively, then edit
them with new values for production.  These files are included in gitignore so secret things won't be
commited to git.

6) Build the production version.  The production version uses nginx and gunicorn, while dev uses the built in python test server.  Look at ~Makefile~ to see other available commmands.  This step will take a while, good time for coffee.
`make prodbuild`

7) Once the build is complete, you should have a list something like this after running `docker images`:

>REPOSITORY                                          TAG                 IMAGE ID            CREATED             SIZE
>ajv_wsdmm_gui_prototype_web                         latest              1f35b869e5d2        4 weeks ago         804MB
><none>                                              <none>              d1249b51d6ae        4 weeks ago         560MB
>ajv_wsdmm_gui_prototype_nginx                       latest              e2e5c4ca8f0e        4 weeks ago         21.2MB
><none>                                              <none>              7e0bd6d46d27        4 weeks ago         804MB
>postgres                                            latest              73119b8892f9        2 months ago        314MB
>python                                              3.8.0-alpine        59acf2b3028c        5 months ago        110MB
>nginx                                               1.17.4-alpine       dfc78cd150cf        6 months ago        21.2MB

8) Export each of the docker images to a tar file:   

`docker save <name>:<tag> -o <name>_<tag>.tar`

9) If you've got a slow network connection that you need to move them, it is helpful to xz compress the files.  Its slow, but quite effective:   

`xz -d *.xz`

10) Run `make deploy` in the ajv_wsdmm_gui_prototype/ directory.  This will create a deploy.tar.gz file, copy it to the target system as well.


## Deploy

1) On the target system, install docker-compose (see Linux tab)
https://docs.docker.com/compose/install/

2) Load the docker images using:
`docker load -i <name>_<tag>.tar[.xz]`

No need to decompress them first - docker load will do that.

3) Untar the deploy.tar.gz file in ~/

`tar zxvf deploy.tar.gz`

4) cd into deploy directory, 'su' to root, run `./deploy.sh`

5) deploy.sh will create a directory called ~/gui, and in it will put a docker-compose file, volume mapped directories and set permissions on them.  It will also run the docker containers, and do some pre-setup things such as create a django superuser.  Just follow the prompts.

6) ???

7) Profit!


