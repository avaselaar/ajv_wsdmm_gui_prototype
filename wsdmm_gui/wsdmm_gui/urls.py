##############################################################################
# Main WSDMM GUI URL Dispatch
#
# COPYRIGHT
# | All rights reserved (c) 2020, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
##############################################################################
"""wsdmm_gui URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('apps.main.urls')),
    path('admin/', admin.site.urls, name='admin'),
    path('shunt/', include('apps.shunt.urls')),
    path('shunt/csv/', include('apps.csvbrowse.urls')),
    path('logcap/', include('apps.logcap.urls')),
    path('tcm/', include('apps.tcm.urls')),
]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
