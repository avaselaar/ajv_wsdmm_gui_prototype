###############################################################################
# Database models for the WSDMM GUI Main Application
#
# COPYRIGHT
# | All rights reserved (c) <CREATION YEAR> ALSTOM, <COUNTRY>
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.db import models

NIFI_TYPE = [
    ('File Based VTI', 'Get VTI data from file'),
    ('Live Data VTI', 'Get VTI data from EIXS over network'),
]


class SiteData(models.Model):
    Railroad = models.CharField(max_length=256)
    Division = models.CharField(max_length=256)
    SubDivision = models.CharField(max_length=256)
    LineSegment = models.CharField(max_length=256)
    Milepost = models.CharField(max_length=256)
    DOTNum = models.CharField(max_length=256)
    StreetName = models.CharField(max_length=256)
    City = models.CharField(max_length=256)
    State = models.CharField(max_length=256)
    Country = models.CharField(max_length=256)

    def __str__(self):
        return self.Railroad + " " \
            + self.Division + " - " \
            + self.SubDivision + " " \
            + self.LineSegment + " " \
            + self.Milepost


class wsdmmService(models.Model):
    # Name of the service
    serviceName = models.CharField(max_length=256)
    # Service Hostname
    hostname = models.CharField(max_length=256)
    # Service Port
    hostport = models.IntegerField()

    def __str__(self):
        return self.serviceName + ' - ' + 'http://' + self.hostname + ':' + str(self.hostport)
