###############################################################################
# The WSDMM GUI main app url page.
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.urls import path
from . import views

app_name = 'main'
urlpatterns = [
    path('', views.index.as_view(), name='index'),
    path('config/location', views.config.as_view(), name='loc_config'),
    path('config/vti', views.vticonfig, name='vti_config'),
    path('config/updatevti', views.updatevti, name='update_vti'),
    path('configerror/', views.configerror, name='configerror'),
    path('status/', views.status, name='status'),
    path('about/', views.about, name='about'),
]
