###############################################################################
# The WSDMM GUI main app gui views page.
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.urls import reverse
from django.shortcuts import render, redirect
from django.views.generic import UpdateView, DetailView
from django.http import HttpResponseRedirect
from .models import SiteData
from common.wsdmmconf import wsdmmconf as wc
from common.wsdmmapi import wsdmmapi as wapi
import psutil
import logging

wsdmm_conf = '/data/infiles/wsdmm.conf'

api = wapi.wsdmmapi('main')
vti_api = wapi.wsdmmapi('vti')

logging.basicConfig(level=logging.DEBUG)


class index(DetailView):
    model = SiteData
    template_name = 'main/index.html'

    def get_object(self):
        ds = wc.wsdmmconf()
        if ds.loadConfig(wsdmm_conf) is True:
            data = ds.getConfig()
            object = SiteData(
                Railroad=data['Railroad'],
                Division=data['Division'],
                SubDivision=data['SubDivision'],
                LineSegment=data['LineSegment'],
                Milepost=data['Milepost'],
                DOTNum=data['DOT#'],
                StreetName=data['StreetName'],
                City=data['City'],
                State=data['State'],
                Country=data['Country'],
            )
            return object
        else:
            return

    def render_to_response(self, context):
        ds = wc.wsdmmconf()
        if ds.loadConfig(wsdmm_conf) is False:
            print("Site not configured. Missing config file")
            return redirect('main:configerror')
        return super().render_to_response(context)


def configerror(request):
    return render(request, 'main/configerror.html')


def status(request):
    context = {
        'cpu': psutil.cpu_percent(),
        'mem': psutil.virtual_memory().percent,
        'disk': psutil.disk_usage('/').percent,
        'partitions': psutil.disk_partitions(),
        'disk_io': psutil.disk_io_counters(),
        'net_io_counters': psutil.net_io_counters(),
        'net_if_addrs': psutil.net_io_counters(),
        # 'uptime': datetime.datetime.fromtimestamp(datetime.now() - psutil.boot_time()).strftime("%Y-%m-%d %H:%M:%S"),
    }
    return render(request, 'main/status.html', context)


class config(UpdateView):
    model = SiteData
    template_name = 'main/config.html'
    fields = (
        'Railroad',
        'Division',
        'SubDivision',
        'LineSegment',
        'Milepost',
        'DOTNum',
        'StreetName',
        'City',
        'State',
        'Country',
    )

    def get_object(self):
        # Load data from the file, instead of database
        ds = wc.wsdmmconf()
        if ds.loadConfig(wsdmm_conf) is True:
            data = ds.getConfig()
            object = SiteData(
                Railroad=data['Railroad'],
                Division=data['Division'],
                SubDivision=data['SubDivision'],
                LineSegment=data['LineSegment'],
                Milepost=data['Milepost'],
                DOTNum=data['DOT#'],
                StreetName=data['StreetName'],
                City=data['City'],
                State=data['State'],
                Country=data['Country'],
            )
            return object
        else:
            return

    def form_valid(self, form):
        ds = wc.wsdmmconf()
        ds.loadConfig(wsdmm_conf)
        object = form.save(commit=False)
        # Divert the data to the configuration file,
        # instead of saving it into the database
        data = {
            'Railroad': object.Railroad,
            'Division': object.Division,
            'SubDivision': object.SubDivision,
            'LineSegment': object.LineSegment,
            'Milepost': object.Milepost,
            'DOT#': object.DOTNum,
            'StreetName': object.StreetName,
            'City': object.City,
            'State': object.State,
            'Country': object.Country,
        }
        if ds.setConfig(data) is True:
            ds.writeConfig(wsdmm_conf)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('main:index')

    def render_to_response(self, context):
        ds = wc.wsdmmconf()
        if ds.loadConfig(wsdmm_conf) is False:
            print("Site not configured. Missing config file")
            return redirect('main:configerror')
        return super().render_to_response(context)


@vti_api.check
def vticonfig(request):
    # Populate the Form
    r = api.get('conf/hw/vti2s')
    data = r.json()
    context = {
        'status': 1,
        'pdispname': 'VTI Configuration',
        'fields': data,
        'processor': 'vti',
    }

    return render(request, 'main/vti.html', context)


@vti_api.check
def updatevti(request):
    url = 'conf/hw/vti2s'
    r = api.get(url)
    fields = r.json()
    for key in fields:
        fields[key] = request.POST[key]
    r = api.postJSON(url, fields)
    return HttpResponseRedirect(reverse('main:vti_config'))


@api.check
def about(request):
    context = {
        'status': 1,
    }
    return render(request, 'main/about.html', context)
