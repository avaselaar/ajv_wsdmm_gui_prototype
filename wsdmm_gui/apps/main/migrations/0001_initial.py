# Generated by Django 3.0.3 on 2020-07-27 14:03

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SiteData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Railroad', models.CharField(max_length=256)),
                ('Division', models.CharField(max_length=256)),
                ('SubDivision', models.CharField(max_length=256)),
                ('LineSegment', models.CharField(max_length=256)),
                ('Milepost', models.CharField(max_length=256)),
                ('DOTNum', models.CharField(max_length=256)),
                ('StreetName', models.CharField(max_length=256)),
                ('City', models.CharField(max_length=256)),
                ('State', models.CharField(max_length=256)),
                ('Country', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='wsdmmService',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serviceName', models.CharField(max_length=256)),
                ('hostname', models.CharField(max_length=256)),
                ('hostport', models.IntegerField()),
            ],
        ),
    ]
