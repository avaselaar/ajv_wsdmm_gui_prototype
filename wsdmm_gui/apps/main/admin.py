###############################################################################
# Main WSDMM Application Model Registration
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.contrib import admin

from .models import SiteData
from .models import wsdmmService

admin.site.register(SiteData)
admin.site.register(wsdmmService)
