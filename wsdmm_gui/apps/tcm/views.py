###############################################################################
# This is the django views modules for the TCM NiFi Django project.  It is
# intended to be used within a django app for managing a NiFi tcm install
# through its REST interface.
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django import forms

from tempus_dominus.widgets import DateTimePicker

from common.wsdmmapi import wsdmmapi as wapi

from datetime import datetime, timedelta
import logging
import re
import json

api = wapi.wsdmmapi('tcm')
dbapi = wapi.wsdmmapi('influx')

logger = logging.getLogger(__name__)


@api.check
def index(request):
    r = api.get('status/service/tcm')
    if r.status_code == 200:
        data = r.json()
        status = {}
        for key in data['status']:
            status[key.replace('_', '')] = data['status'][key]
        context = {
            'hostname': data['hostname'],
            'port': data['hostport'],
            'service': data['service'],
            'status': status
        }
    else:
        context = {
            'hostname': 'none',
            'port': 'none',
            'service': 'none'
        }
    return render(request, 'tcm/index.html', context)


@api.check
def config(request, processor_name=''):
    track_ids = api.getTrackIDs('tcm')

    if processor_name == '':
        processor_name = track_ids[0]

    # Populate the processor List
    r = api.get('conf/tcm')
    data = r.json()
    nav = {}
    for item in data.items():
        nav[item[0]] = item[1]['name']

    # Populate the Form
    if len(processor_name) == 3 and re.match('[1-9]_[1-2]', processor_name):
        r = api.get('conf/tcm/track/' + processor_name)
    else:
        r = api.get('conf/tcm/mqtt/' + processor_name)
    data = r.json()
    context = {
        'status': 1,
        'track_ids': track_ids,
        'nav': nav,
        'pdispname': nav[processor_name],
        'fields': data,
        'processor': processor_name,
    }
    return render(request, 'tcm/config.html', context)


@api.check
def update(request):
    processor_name = request.POST['processor']
    url, name = api.pURLName(processor_name)
    r = api.get(url)
    fields = r.json()
    for key in fields:
        fields[key] = request.POST[key]
    r = api.postJSON(url, fields)
    return HttpResponseRedirect(reverse('tcm:config'))


@api.check
def startStop(request):
    if request.POST['control'] == "start":
        api.get('sched/tcm?desired_state=True')
    if request.POST['control'] == "stop":
        api.get('sched/tcm?desired_state=False')
    return HttpResponseRedirect(reverse('tcm:index'))


@api.check
@dbapi.check
def plot(request):
    # Empty dict for data
    data = {
        'meanCurrent': [],
        'operating_margin_max': [],
        'operating_margin_min': [],
        'blowby_threshold': [],
        'dropout_threshold': [],
        'reliability_margin': [],
        'safety_margin': [],
    }
    track_ids = api.getTrackIDs('tcm')

    # Start time/end time in datetime objects
    # IF this is in service in 2038, something has gone horribly wrong.
    time_format_string = "%Y-%m-%d %H:%M"
    timenow = datetime.today()
    if 'Begin' in request.GET:
        start_time = datetime.strptime(request.GET['Begin'], time_format_string)
    else:
        start_time = timenow - timedelta(seconds=24 * 60 * 60)  # default 1 day

    logger.debug(start_time)

    if 'End' in request.GET:
        end_time = datetime.strptime(request.GET['End'], time_format_string)
    else:
        end_time = timenow

    if 'track' in request.GET:
        track = request.GET['track']
    else:
        track = track_ids[0]

    # Which track do we want data for, and submit query
    if track in track_ids:
        r = api.get(str.format('data/tcm?startTime={}&endTime={}&track={}', int(start_time.timestamp()), int(end_time.timestamp()), track))
        if r.status_code == 200:
            plotdata = json.loads(r.json())
            for series in data:
                for x, value in plotdata.items():
                    data[series].append({'x': int(x) / 1000, 'y': value[series]})
        else:
            data = None

    form = dateForm()

    initial_date = {
        'start_time': start_time.strftime("%Y-%m-%d %H:%M"),
        'end_time': end_time.strftime("%Y-%m-%d %H:%M"),
    }

    context = {
        'track': track,
        'track_ids': track_ids,
        'initial_date': initial_date,
        'data': data,
        'form': form,
    }

    return render(request, 'tcm/plot.html', context)


class dateForm(forms.Form):
    Begin = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'format': "YYYY-MM-DD HH:mm",
                'collapse': False,
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            }
        ),
    )

    End = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'format': "YYYY-MM-DD HH:mm",
                'collapse': False,
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            }
        ),
    )
