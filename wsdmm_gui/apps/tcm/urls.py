###############################################################################
# TCM Application URL Dispatch
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.urls import path
from . import views

app_name = 'tcm'
urlpatterns = [
    path('', views.index, name='index'),
    path('config/', views.config, name='config'),
    path('config/update', views.update, name='update'),
    path('config/startstop', views.startStop, name='startstop'),
    path('config/<str:processor_name>', views.config, name='config'),
    path('plot/', views.plot, name='plot'),
]
