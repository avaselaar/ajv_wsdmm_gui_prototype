###############################################################################
# The data model for the LogCap django app.
#
# The goal here is to provide just enough information for django to interact
# with the shunt NiFi app - nothing more.  All other data is stored in NiFi.
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.db import models


class LogCapServer(models.Model):
    # Hostname of the target LogCap instance
    hostname = models.CharField(max_length=256)
    # Port of the target LogCap instance
    hostport = models.IntegerField(default=8080)
    # Max Agg Log length (bytes)
    maxagglog = models.FloatField(default=10000000)

    def __str__(self):
        return self.hostname + ":" + str(self.hostport)
