###############################################################################
# This is the django views modules for the LogCap Django project.  It is
# intended to be used within a django app for managing a LogCap install
# through its REST interface.
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, FileResponse
from django.urls import reverse
from django import forms

from tempus_dominus.widgets import DateTimePicker
from fontawesome_5.fields import IconField

from .models import LogCapServer
from .pylogcap import pylogcap

import io
import re
import zipfile
import logging
from datetime import datetime, timedelta

logger = logging.getLogger(__name__)


###############################################################################
#                                View Functions                               #
###############################################################################
def index(request):
    """ Status page """
    lm = lc_init()
    hostname, port = lm.getSocket()
    context = {
        'hostname': hostname,
        'port': port,
        'status_data': lm.getStatus(),
        'lm_settings': lm.getSettings(),
        'socket_status': lm.getSocketStatus()
    }
    return render(request, 'logcap/index.html', context)


def config(request, device_type='', device_name=''):
    """ Configure a device in logcap"""
    lm = lc_init()

    if lm != -1:
        hostname, port = lm.getSocket()
        device_list = lm.getDeviceList()
        status = 1
        # Check if name and device type are valid
        if device_name != '':
            if lm.checkDeviceName(device_type, device_name) is False:
                logger.error("Invalid device:" + device_name)
                return HttpResponseRedirect(reverse('logcap:config'))

        if device_type != '':
            if lm.checkDeviceType(device_type) is False:
                logger.error("Invalid device type:" + device_type)
                return HttpResponseRedirect(reverse('logcap:config'))

        if device_name == '':
            form_elements = lm.getSettings()
            device_name = 'LogCap'
            device_type = 'logcap'
        else:
            form_elements = lm.getDevice(device_type, device_name)

        (dict_list, list_list, string_list, num_list) = settingsToFormElements(form_elements)
    else:
        logger.info("LogCap Unavailable")
        status = 0
        context = {'device_list': ""}

    context = {
        'device_list': device_list,
        'device_name': device_name,
        'device_type': device_type,
        'supported_device_types': lm.getSupportedTypes(),
        'form_elements': form_elements,
        'dict_list': dict_list,
        'list_list': list_list,
        'string_list': string_list,
        'num_list': num_list,
        'status': status
    }

    return render(request, 'logcap/config.html', context)


def logs(request, timeframe=''):
    """ Get logs from the aggregated log endpoint"""
    log_list = ''
    request_list = ''
    status = 0
    lm = lc_init()  # Initialize class for communicating with logcap
    end_time = datetime.today()
    start_time = end_time - timedelta(seconds=24 * 60 * 60)  # default 1 day

    if lm != -1:
        devices = lm.getDeviceList()  # Get available eixs units
        log_tree = makeLogIndex(lm, devices)  # Get list of logs for each unit

        if request.POST:
            # Figure out our time window
            if 'Begin' in request.POST and request.POST['Begin'] != '':
                start_time = datetime.strptime(request.POST['Begin'], "%Y-%m-%d %H:%M")

            if 'End' in request.POST and request.POST['End'] != '':
                end_time = datetime.strptime(request.POST['End'], "%Y-%m-%d %H:%M")

            if 'logs' in request.POST and request.POST['logs'] != '':
                request_list = request.POST['logs'].strip('][').split(',')  # Get the selected logs
                try:
                    request_list = list(map(int, request_list))  # Convert it to ints
                except Exception as e:
                    logger.error("Failed: " + str(e) + " \n")  # List was empty
                    logger.error(request_list)
                log_list = ''
                for device in log_tree:  # Go through each device
                    requested_logs = []
                    for log_type in device['logtypes']:  # Make a list of the logs that were requested
                        id_num = int(log_type['id'])
                        if id_num > 0 and id_num in request_list:  # Devices themselves have negative id's so are excluded
                            requested_logs.append(log_type['logtype'])
                    # Dump all the logs into a big fat string
                    if requested_logs != []:
                        temp = lm.getAggLog(device['type'], device['text'], start_time, end_time, requested_logs)
                        if temp != '[]' and temp is not None:  # Check for empty list
                            temp = temp.strip('[]')  # Combine it with the larger list
                            temp = temp.replace('name', 'data')  # Fix for a bug in LJ - errorLog
                            temp = temp + ','
                            log_list = log_list + temp
                            status = status + 1
                        # TOO MUCH DATA - ABORT!
                        if len(log_list) > lm.getMaxLog():
                            status = -2
                            log_list = ''
                            break
            log_list = '[' + log_list + ']'  # Re-JSON it
        else:
            log_list = '[]'  # Nothing requested

        initial_date = {
            'start_time': start_time.strftime("%Y-%m-%d %H:%M"),
            'end_time': end_time.strftime("%Y-%m-%d %H:%M"),
        }
        date_form = dateForm()
        context = {
            'log_list': log_list,
            'log_tree': log_tree,
            'request_list': request_list,
            'status': status,
            'form': date_form,
            'initial_date': initial_date,
            'icon': IconField()
        }
        return render(request, 'logcap/logs.html', context)


def showDeviceConf(request, device_type='', device_name=''):
    """ Shows the deviceconf for a given device"""
    lm = lc_init()

    device_list = lm.getDeviceList()

    if len(device_list) > 0:
        if device_name == '':
            device_name = device_list[0][1]   # Get the first device
            device_type = device_list[0][0]

    config = lm.getDeviceConfig(device_type, device_name)

    if config is not None:
        config = {logNameToTitle(key): value for key, value in config.items()}
    else:
        config = ''

    context = {'config': config,
               'device_list': device_list}

    return render(request, 'logcap/deviceconf.html', context)


def addDeviceForm(request, device_type):
    """ Form for adding a new device to logcap """
    lm = lc_init()

    config = lm.getDefaultConfig(device_type)

    (dict_list, list_list, string_list, num_list) = settingsToFormElements(config['settings'])

    context = {'form_elements': config['settings'],
               'dict_list': dict_list,
               'list_list': list_list,
               'string_list': string_list,
               'device_type': device_type,
               'num_list': num_list}

    return render(request, 'logcap/adddeviceform.html', context)


def addDevice(request):
    """Add a new device to a logcap server"""
    lm = lc_init()
    device_name = request.POST['device_name']
    device_type = request.POST['device_type']
    config_data = lm.getDefaultConfig(device_type)
    config_data['name'] = device_name
    config_data['settings'] = POSTtoConfig(request.POST, config_data['settings'])
    lm.addDevice(device_type, config_data)
    return HttpResponseRedirect(reverse('logcap:config'))


def delDevice(request, device_type, device_name):
    """Remove a device from a server"""
    lm = lc_init()
    lm.delDevice(device_type, device_name)
    return HttpResponseRedirect(reverse('logcap:config'))


def POSTtoConfig(post_data, config_data):
    """Update a config_data structure from a POST only if the config_data
    contains an appropriate key"""
    for key in config_data:
        if isinstance(config_data[key], dict):
            for inkey in config_data[key]:
                if post_data[key + '.' + inkey] != config_data[key][inkey]:
                    the_type = type(config_data[key][inkey])
                    config_data[key][inkey] = the_type(post_data[key + '.' + inkey])
        elif isinstance(config_data[key], list):
            config_data[key] = []
            temp = post_data[key].replace(" ", "")
            config_data[key] = temp.split(',')
        else:
            the_type = type(config_data[key])
            config_data[key] = the_type(post_data[key])
    return config_data


def update(request):
    """ Update device configuration on LogCap Server"""
    lm = lc_init()

    # Get the state of the lumberjack
    status_data = lm.getStatus()

    # What state is lumberjack in?
    if status_data['state'] == 'Unconfigured':
        device_name = 'LogCap'
        config_data = lm.getSettings()
    else:
        # Get device information to update
        device_name = request.POST['device_name']
        device_type = request.POST['device_type']
        # Check if name and device type are valid
        if lm.checkDeviceName(device_type, device_name) is False:
            logger.error("Invalid device:" + device_name)
            return HttpResponseRedirect(reverse('logcap:config'))
        else:
            config_data = lm.getDevice(device_type, device_name)

    # Update the config data - Only fill in variables that are actually available
    for key in config_data:
        if isinstance(config_data[key], dict):
            for inkey in config_data[key]:
                if request.POST[key + '.' + inkey] != config_data[key][inkey]:
                    the_type = type(config_data[key][inkey])
                    config_data[key][inkey] = the_type(request.POST[key + '.' + inkey])
        elif isinstance(config_data[key], list):
            config_data[key] = []
            temp = request.POST[key].replace(" ", "")
            config_data[key] = temp.split(',')
        else:
            the_type = type(config_data[key])
            config_data[key] = the_type(request.POST[key])

    # If we're setting up a fresh LJ instance, it is a POST, else it is a PUT
    if status_data['state'] == 'Unconfigured':
        logger.debug(lm.postSettings(config_data))
    else:
        logger.debug(lm.putDevice(device_type, device_name, config_data))

    # Take us back to our config page
    return HttpResponseRedirect(reverse('logcap:config'))


def startStop(request):
    """ Start/stop logcap"""
    lm = lc_init()
    if request.POST['control'] == "start":
        lm.schedule(True)
    if request.POST['control'] == "stop":
        lm.schedule(False)
    return HttpResponseRedirect(reverse('logcap:index'))


def listRawLogs(request, device_type='', device_name='', filename=''):
    """ Create a list of raw logs per device"""
    lm = lc_init()
    device_list = lm.getDeviceList()

    if len(device_list) > 0:
        if device_name == '':
            device_name = device_list[0][1]   # Get the first device
            device_type = device_list[0][0]

    log_list = lm.getAvailLogs(device_type, device_name)

    log_menu = {}

    if log_list is not None:
        for log_type in log_list['archiveInfo']:
            name = logNameToTitle(log_type['logEntries'][0].split('.')[1])
            log_menu[name] = log_type['logEntries'][0]

    if filename != '':
        log = lm.getLog(device_type, device_name, filename)
        log_type = re.sub(r'((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))', r' \1', filename.split('.')[1])
        log_type = log_type.title()
    else:
        log_type = ''
        log = ''

    context = {
        'config': config,
        'device_list': device_list,
        'device_name': device_name,
        'device_type': device_type,
        'log_menu': log_menu,
        'log_type': log_type,
        'filename': filename,
        'log': log,
    }

    return render(request, 'logcap/rawlogs.html', context)


def rawLog(request, device_type='', device_name='', filename=''):
    """ Display a raw log """
    lm = lc_init()
    log = lm.getLog(device_type, device_name, filename)
    return HttpResponse(log, content_type='text/plain')


def dlRawLog(request, device_type='', device_name='', filename=''):
    """ Download a single raw log """
    lm = lc_init()
    log = lm.getLog(device_type, device_name, filename)
    response = HttpResponse(log, content_type='application/text charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename=' + filename
    return response


def dlLatestRaw(request, device_type='', device_name=''):
    """ Download raw logs as a zip file """
    lm = lc_init()
    log_list = lm.getAvailLogs(device_type, device_name)
    log_menu = {}
    for log_type in log_list['archiveInfo']:
        name = log_type['logEntries'][0].split('.')[1]
        log_menu[name] = log_type['logEntries'][0]

    zipbuffer = io.BytesIO()  # Make an in-memory file buffer for zipfile
    now = datetime.now()    # Generate a timestamp
    name = now.strftime("%Y%m%d_%H%M%S") + "_" + device_name + "_logs.zip"
    try:  # Make a zipfile
        zf = zipfile.ZipFile(zipbuffer, mode='w', compression=zipfile.ZIP_DEFLATED)
        for logtype, logname in log_menu.items():
            log = lm.getLog(device_type, device_name, logname)
            logger.info("Zipping... " + logname)
            zf.writestr(logname, log)
        zf.close()
        zipbuffer.seek(0)
        response = FileResponse(zipbuffer, as_attachment=True, filename=name)
    except Exception as e:
        logger.error("Unable to create zip file " + name + " " + str(e))
        response = HttpResponseRedirect(reverse('logcap:rawlogs'))
    return response


###############################################################################
#                                   Classes                                   #
###############################################################################
class dateForm(forms.Form):
    """ Class for the tempus-dominus datepicker """
    Begin = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'collapse': False,
                'format': "YYYY-MM-DD HH:mm"
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            }
        ),
    )

    End = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'collapse': False,
                'format': "YYYY-MM-DD HH:mm"
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            }
        ),
    )


###########################################################################
#                             Helper Functions                            #
###########################################################################
def logNameToTitle(name):
    """ Turn a camel-case name into a title with spaces. """
    output = re.sub(r'((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))', r' \1', name)
    output = output.title()
    output = output.replace('Ptc', 'PTC')
    if 'Piwp' in output:
        output = 'GDA Log'

    return output


def makeLogIndex(lm, devices):
    """ Make an index of the logs that are allowed for the aggregated endpoint and are available for a given device"""
    log_tree = []
    if lm != -1:
        pk = 0
        for device in devices:
            tmp_children = []
            log_types = lm.getAvailAggLogTypes(device[0], device[1])
            for log in log_types:
                tmp_children.append({'text': logNameToTitle(log), 'logtype': log, 'id': pk})
                pk = pk + 1
            log_tree.append({'text': device[1], 'type': device[0], 'id': pk * -1, 'haschildren': 'true', 'logtypes': tmp_children})
            pk = pk + 1
        return log_tree
    else:
        return ''


def settingsToFormElements(settings):
    """ Make a set of forms for device settings"""
    dict_list = []
    list_list = []
    string_list = []
    num_list = []

    if settings:
        for key in settings:
            if isinstance(settings[key], dict):
                dict_list.append(key)
            elif isinstance(settings[key], list):
                list_list.append(key)
            elif isinstance(settings[key], (float, int)):
                num_list.append(key)
            else:
                string_list.append(key)
    return (dict_list, list_list, string_list, num_list)


def lc_init():
    """ Intialize the pylogcap module to talk to a server"""
    try:
        host = LogCapServer.objects.get()  # Grab connection info from db
    except Exception as e:
        # On a new install, there won't be an entry in the DB for the LogCap
        # instance.
        # We'll give it a sane default:
        logger.error(str(e))
        logger.error("No server configured, adding default server.")
        defaulthost = LogCapServer(
            hostname="lumberjack_restapi",
            hostport=8080,
        )
        defaulthost.save()
        host = LogCapServer.objects.get()  # Grab connection info from db

    try:
        lm = pylogcap.lmClient(host.hostname, host.hostport)
        lm.setMaxLog(host.maxagglog)

    except Exception as e:
        logger.error("LogCap Unavailable" + str(e))
    return lm
