from django.apps import AppConfig


class LogcapConfig(AppConfig):
    name = 'logcap'
