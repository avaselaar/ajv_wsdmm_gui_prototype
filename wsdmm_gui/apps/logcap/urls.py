###############################################################################
#  URL Dispatch for LogCap app
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.urls import path
from . import views

app_name = 'logcap'
urlpatterns = [
    path('', views.index, name='index'),
    path('config', views.config, name='config'),
    path('update', views.update, name='update'),
    path('logs/<int:timeframe>', views.logs, name='logs'),
    path('logs/', views.logs, name='logs'),
    path('config/startstop', views.startStop, name='startstop'),
    path('adddevice', views.addDevice, name='adddevice'),
    path('deldevice/<str:device_type>/<str:device_name>', views.delDevice, name='deldevice'),
    path('deviceconf/', views.showDeviceConf, name='deviceconf'),
    path('deviceconf/<str:device_type>/<str:device_name>', views.showDeviceConf, name='deviceconf'),
    path('adddeviceform/<str:device_type>', views.addDeviceForm, name='adddeviceform'),
    path('config/<str:device_type>/<str:device_name>', views.config, name='config'),
    path('rawlogs/', views.listRawLogs, name='rawlogs'),
    path('rawlogs/<str:device_type>/<str:device_name>', views.listRawLogs, name='rawlogs'),
    path('rawlogs/<str:device_type>/<str:device_name>/<str:filename>', views.listRawLogs, name='rawlogs'),
    path('rawlog/<str:device_type>/<str:device_name>/<str:filename>', views.rawLog, name='rawlog'),
    path('dlrawlog/<str:device_type>/<str:device_name>/<str:filename>', views.dlRawLog, name='dlrawlog'),
    path('dllatestraw/<str:device_type>/<str:device_name>', views.dlLatestRaw, name='dllatestraw'),
]
