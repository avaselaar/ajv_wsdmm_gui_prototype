#!/usr/bin/env python3
###############################################################################
# This is the a module for supporting logcap.  It is intended for use with
# django, but it could be used for other things too.
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
import json
import requests
import copy
import logging

logger = logging.getLogger(__name__)

supported_device_types = ['eixs', 'vhlc']

# Logs that we can get through the aggregated interface.
agg_logs = ['alarmLog',
            'configLog',
            #  'dataLog', # REmoved datalog because I can't display it yet
            'errorLog',
            'ringStatusLog',
            'trainRecordLog',
            'trainDataLog',
            'systemEventLog',
            'ptcEventLog',
            'ptcCommLog',
            'ptcDeviceStateLog']

# Default configurations for each device and logcap
default_configs = {
    'logcap': {
        "name": "default",
        "settings": {
            "mqttBrokerAddress": "127.0.0.1",
            "pausedState": False,
        },
    },
    'eixs': {
        'name': 'string',
        'settings': {
            "eixsGroupId": "string",
            "eixsIpAddress": "192.168.88.4",
            "chassisType": "eixs",
            "telnetSpecs": {
                "username": "admin",
                "password": "telnet",
                "portNum": 23
            },
            "sshSpecs": {
                "username": "admin",
                "password": "sshuser",
                "portNum": 22
            },
            "logRetrievalRate": 3600,
            "logRetrievalWindowStartTime": "00:00:00",
            "logRetrievalWindowEndTime": "00:00:00",
            "logArchiveSizeInDays": 30,
            "snmpPort": 161,
            "alertingKeywords": [
                "keywordDetectedInLog"
            ]
        }
    },
    'vhlc': {
        "name": "labVhlc",
        "settings": {
            "vhlcGroupId": "string",
            "vhlcConnectionParams": {
                "ipAddress": "192.168.10.225",
                "port": 50001,
                "username": "",
                "password": "har"
            },
            "logRetrievalRate": 3600,
            "logRetrievalWindowStartTime": "00:00:00",
            "logRetrievalWindowEndTime": "00:00:00",
            "logArchiveSizeInDays": 30,
            "alertingKeywords": [
                "string"
            ]
        }
    },
    'sear': {
        "name": "<string>",
        "settings": {
            "searGroupId": "<string>",
            "searIpAddress": "<string>",
            "searUsername": "<string>",
            "searPassword": "<string>",
            "logRetrievalRate": "<integer>",
            "logArchiveSizeInDays": "<integer>",
            "notifications": [
                {
                    "key": "<string>",
                    "value": "<boolean>"
                },
                {
                    "key": "<string>",
                    "value": "<boolean>"
                }
            ]
        }
    },
}


class lmClient:
    def __init__(self, hostname, port):
        self.setSocket(hostname, port)
        self.getSocketStatus()

    def setSocket(self, hostname, port):
        """ Generates a base url for talking to logcap"""
        self.hostname = hostname
        self.port = port
        self.api_url_base = "http://" + self.hostname + ":" + str(self.port) + "/v1/"

    def getSocket(self):
        """ Returns socket information for this instance of lmClient """
        return self.hostname, self.port

    def getSocketStatus(self):
        """  Dumb Connection state """
        if self.getStatus == '':
            self.socket_status = False
        else:
            self.socket_status = True
        return self.socket_status

    def setMaxLog(self, max_log):
        """ Set the maximum size of the log returned """
        self.max_log = max_log

    def getMaxLog(self):
        """ Return the currently set max log size"""
        return self.max_log

    def printSocket(self):
        """ Print socket information to log """
        logger.info("H: " + self.hostname)
        logger.info("P: " + str(self.port))
        logger.info(self.api_url_base)
        return self.api_url_base

    def getStatus(self):
        """ Get LogCap status"""
        return self.getJSON(
            '{0}lumberjack/status'.format(self.api_url_base))

    def getSettingsAll(self):
        """ Get logcap settings/all """
        return self.getJSON(
            '{0}lumberjack/settings/all'.format(self.api_url_base))

    def getSettings(self):
        """ Get locagpsettings """
        temp = self.getJSON(
            '{0}lumberjack/settings'.format(self.api_url_base))
        if not temp:
            return copy.deepcopy(default_configs['logcap'])
        else:
            return temp

    def putSettings(self, settings_data):
        """ Put (replace) a new set of settings to logcap """
        return self.putJSON(
            '{0}lumberjack/settings'.format(self.api_url_base), settings_data)

    def postSettings(self, settings_data):
        logger.debug(json.dumps(settings_data))
        """ Initiate new settings to logcap """
        return self.postJSON(
            '{0}lumberjack/settings'.format(self.api_url_base), settings_data)

    def getAvailLogs(self, device_type, device_name):
        """ Get the logs available for a device"""
        return self.getJSON(
            '{0}lumberjack/archive/raw/{1}/status/{2}'
            .format(self.api_url_base, device_type, device_name))

    def getLog(self, device_type, device_name, filename):
        """ Get a single raw log file """
        return self.getTXT(
            '{0}lumberjack/archive/raw/{1}/{2}/{3}'
            .format(self.api_url_base, device_type, device_name, filename))

    def getDevice(self, device_type, device_name):
        """ Get configuration for a single device"""
        if self.checkDeviceType(device_type) is True:
            response = self.getJSON(
                '{0}lumberjack/settings/{1}/{2}'.format(
                    self.api_url_base, device_type, device_name))
            return response
        else:
            return "{}"

    def getAvailAggLogTypes(self, device_type, device_name):
        """ Get the log types available for a given device """
        avail_logs = self.getAvailLogs(device_type, device_name)
        avail_log_types = []
        for log_type in avail_logs['archiveInfo']:
            if log_type['logType'] in agg_logs:
                avail_log_types.append(log_type['logType'])
        return avail_log_types

    def getAggLog(self, device_type, device_name, start_datetime, end_datetime, get):
        """Get list of logs available for device and type """
        avail_logs = self.getAvailLogs(device_type, device_name)
        log_type_list = []
        for item in avail_logs['archiveInfo']:
            if item['logType'] not in log_type_list:
                log_type_list.append(item['logType'])

        # Format time
        output_time_format = "%Y-%m-%dT%H:%M:%S.000Z"
        start = start_datetime.strftime(output_time_format)
        end = end_datetime.strftime(output_time_format)

        # Build a get parameter string
        get_string = ''
        for item in get:
            if item in log_type_list:
                get_string = get_string + '&' + item + '=true'

        url = '{0}lumberjack/archive/aggregated/{1}/{2}?startTime={3}&endTime={4}'.format(self.api_url_base, device_type, device_name, start, end)
        url = url + get_string
        return self.getTXT(url)

    def addDevice(self, device_type, device_data):
        """ Add a new device to logcap """
        # TODO Add some checking to make sure we're not sending invalid things
        if self.checkDeviceType(device_type) is True:  # Is the device a valid device?
            if device_data['name'] != "":
                response = self.postJSON(
                    '{0}lumberjack/settings/{1}'.format(
                        self.api_url_base, device_type), device_data)
                logger.debug(response)  # What did it say?!
            else:
                logger.error("Invalid device name: " + device_data['name'])
        else:
            logger.error("Invalid device type: " + device_type)

    def delDevice(self, device_type, device_name):
        """ Delete a device from logcap"""
        if self.checkDeviceType(device_type) is True:
            return self.delete(
                '{0}lumberjack/settings/{1}/{2}'.format(
                    self.api_url_base, device_type, device_name))

    def configLogCap(self, lm_data):
        """ Initalize logcap with settings """
        response = self.postJSON('{0}lumberjack/settings'.format(self.api_url_base), lm_data)
        logger.debug(response)  # What did it say?!

    def putDevice(self, device_type, device_name, device_data):
        """ Replace settings for a device in logcap """
        if self.checkDeviceType(device_type) is True:
            return self.putJSON(
                '{0}lumberjack/settings/{1}/{2}'.format(
                    self.api_url_base, device_type, device_name), device_data)

    def getDeviceConfig(self, device_type, device_name):
        """ Get a configuration for a device in logcap """
        if self.checkDeviceType(device_type) is True:
            return self.getJSON(
                '{0}{1}/config/{2}'.format(
                    self.api_url_base, device_type, device_name))

    def checkDeviceType(self, device_type):
        """ Find if a device is supported """
        status = False
        if device_type in supported_device_types:
            status = True
        elif device_type == 'logcap':
            status = True
        return status

    def getJSON(self, url):
        """ Download a thing, return as JSON"""
        logger.debug(url)
        try:
            response = requests.get(url)
            if response.status_code == 200:
                return json.loads(response.content.decode('utf-8'))
            else:
                logger.debug("Response: " + str(response.status_code))
        except Exception as e:
            logger.error(str(e))
            return ''

    def getTXT(self, url):
        """ Download a thing, return as TXT"""
        logger.debug(url)
        try:
            response = requests.get(url)
            if response.status_code == 200:
                return response.content.decode('utf-8')
            else:
                logger.debug("Response: " + str(response.status_code))
        except Exception as e:
            logger.error(str(e))
            return ''

    def postJSON(self, url, data):
        """ Post a JSON payload """
        try:
            response = requests.post(url, json=data)
            if response.status_code == 200:
                return response.content.decode('utf-8')
            else:
                logger.debug("Response: " + str(response))
        except Exception as e:
            logger.error(str(e))
            return ''

    def putJSON(self, url, data):
        """ Put a JSON Payload"""
        try:
            response = requests.put(url, json=data)
            if response.status_code == 200:
                return response.content.decode('utf-8')
            else:
                logger.debug("Response: " + str(response.status_code))
        except Exception as e:
            logger.error(str(e))
            return ''

    def delete(self, url):
        """ Send an HTTP DEL """
        logger.debug(url)
        try:
            response = requests.delete(url)
            status = False
            if response.status_code == 200:
                logger.debug("Delete " + url + " success")
                status = True
            else:
                logger.error("Delete " + url + " failed")
            return status
        except Exception as e:
            logger.error(str(e))
            return ''

    def getDefaultConfig(self, device_type):
        """ Get a copy of a default config for a device type"""
        if self.checkDeviceType(device_type) is True:
            return copy.deepcopy(default_configs[device_type])

    def getDeviceList(self):
        """ GEnerate a list of all active devices on a logcap """
        settings = self.getSettingsAll()
        device_list = []

        if settings is not None:
            for device_type in supported_device_types:
                if device_type in settings:
                    for device in settings[device_type]:
                        if device['name'] != "":
                            device_list.append((device_type, device['name']))
        return device_list

    def checkDeviceName(self, device_type, device_name):
        """ Detmine that a device name is not a name of logcap """
        device_list = self.getDeviceList()
        status = False
        if (device_type, device_name) in device_list:
            status = True
        elif device_name == 'LogCap':
            status = True
        return status

    def schedule(self, state):
        """ Pause logcap """
        config_data = self.getSettings()
        if state is True:
            config_data['pausedState'] = False
        else:
            config_data['pausedState'] = True
        self.putSettings(config_data)

    def getSupportedTypes(self):
        """ Return list of supported device types """
        return supported_device_types


def main():
    print("LogCap Python Client v0.1")
    lm = lmClient("192.168.88.8", 8080)
    lm.printSocket()
#    print(lm.getStatus())
#    lm_settings = lm.getSettingsAll()
#    print(lm_settings)
#    empty = lm.getDefaultConfig('eixs')
#    empty['name'] = 'notaneixs'
#    empty['settings']['eixsIpAddress'] = '192.168.1.10'
#    lm.addDevice('eixs', empty)
    print(lm.getDeviceList())
#    lm.delDevice('eixs', 'notaneixs')
#    lm.delDevice('eixs', 'string')


if __name__ == "__main__":
    main()
