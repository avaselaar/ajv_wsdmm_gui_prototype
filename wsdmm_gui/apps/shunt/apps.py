###############################################################################
# Shunt Django Application Registration
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.apps import AppConfig


class ShuntConfig(AppConfig):
    name = 'shunt'
