###############################################################################
# This is the django views modules for the Shunt NiFi Django project.  It is
# intended to be used within a django app for managing a NiFi shunt install
# through its REST interface.
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

from common.wsdmmapi import wsdmmapi as wapi

import logging
import re

api = wapi.wsdmmapi('shunt')

logging.basicConfig(level=logging.DEBUG)


@api.check
def index(request):
    r = api.get('status/service/shunt')
    if r.status_code == 200:
        data = r.json()
        status = {}
        for key in data['status']:
            status[key.replace('_', '')] = data['status'][key]
        context = {
            'hostname': data['hostname'],
            'port': data['hostport'],
            'service': data['service'],
            'status': status
        }
    else:
        context = {
            'hostname': 'none',
            'port': 'none',
            'service': 'none'
        }
    return render(request, 'shunt/index.html', context)


@api.check
def config(request, processor_name=''):

    track_ids = api.getTrackIDs('shunt')
    if track_ids == -1:
        return render(request, 'shunt/config.html', '')

    if processor_name == '':
        processor_name = track_ids[0]
    # Populate the processor List
    r = api.get('conf/shunt')
    data = r.json()
    nav = {}
    for item in data.items():
        nav[item[0]] = item[1]['name']
    # Populate the Form
    if len(processor_name) == 3 and re.match('[1-9]_[1-2]', processor_name):
        r = api.get('conf/shunt/track/' + processor_name)
    else:
        r = api.get('conf/shunt/mqtt/' + processor_name)
    data = r.json()
    context = {
        'status': 1,
        'nav': nav,
        'pdispname': nav[processor_name],
        'fields': data,
        'processor': processor_name,
    }
    return render(request, 'shunt/config.html', context)


@api.check
def update(request):
    processor_name = request.POST['processor']
    url, name = api.pURLName(processor_name)
    r = api.get(url)
    fields = r.json()
    for key in fields:
        fields[key] = request.POST[key]
    r = api.postJSON(url, fields)
    return HttpResponseRedirect(reverse('shunt:config'))


@api.check
def startStop(request):
    if request.POST['control'] == "start":
        api.get('sched/shunt?desired_state=True')
    if request.POST['control'] == "stop":
        api.get('sched/shunt?desired_state=False')
    return HttpResponseRedirect(reverse('shunt:index'))
