###############################################################################
#  URL Dispatch for CSV Browser app
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.urls import path
from . import views

app_name = 'csvbrowse'
urlpatterns = [
    path('', views.indexView.as_view(), name='index'),
    path('<int:pk>/', views.csvView, name='detail'),
    path('download/<str:pk>/', views.downloadFile, name='download'),
    path('delete/<str:pk>/', views.deleteFile, name='delete'),
    path('chk/', views.chkBoxHandler, name='chkhandle'),
]
