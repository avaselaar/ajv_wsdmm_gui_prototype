#!/usr/bin/env python3
###############################################################################
# Utility functions for CSV Browse
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
import os
import io
import glob
import re
import shutil
import datetime


def enumerateFiles(path, site_data):
    """ Get a list of files in a path.

        Also, renames files on the drive so they don't have colons and conform
        to however it is we want them named.
    """
    # Incoming file
    # EIXS chassis ID_5_1_train_move_2020-03-13 18:03:26.330Z.csv
    # <PLATFORM ID>_<SLOT>_<TRACK NO>_train_move_YYYY-mm-DD HH:MM:SS.fZ.csv
    # Outgoing File
    # YYYYmmDD-HH-MM-SS_<RR>_<SUB>_<MP>_<CHASSIS_ID>_<SLOT>_<TRACK>.csv
    #
    # Fresh Train Moves
    train_moves = glob.glob(path + '/EIXS*.csv')
    for move in train_moves:
        basename = os.path.basename(move)
        pieces = basename.split('_')
        datestamp = pieces[5].replace('.csv', '').replace(' ', '_')
        new_name = datestamp + '_' \
            + site_data.Division + '_' \
            + site_data.SubDivision + '_' \
            + site_data.Milepost + '_' \
            + pieces[0] + '_' \
            + pieces[1] + '_' \
            + pieces[2]
        new_name = getValidFilename(new_name) + '.csv'
        shutil.move(move, path + '/' + new_name)

    # Renamed train moves are what we want
    train_moves = glob.glob(path + '/' + '*.csv')
    return train_moves


def getValidFilename(s):
    s = str(s).strip().replace(':', '.')
    return re.sub(r'(?u)[^-\w.]', '', s)


def parseName(name):
    temp_name = name.rsplit('/', 1)[1]   # Get rid of filepath
    temp_name = temp_name.rsplit('.', 1)[0]  # Get rid of CSV ext
    parts = temp_name.split('_')  # Split at _'s
    data = {
        'division': parts[2],
        'subdivision': parts[3],
        'milepost': parts[4],
        'platform_id': parts[5],
        'slot': parts[6],
        'track': parts[7],
        'date': datetime.datetime.strptime(parts[0] + "_" + parts[1], '%Y-%m-%d_%H.%M.%S.%fZ')}
    return data


def deleteFile(filename):
    """ Delete a file.
    """
    try:
        os.remove(filename)
        print("Deleted: " + filename)
        status = True
    except Exception as e:
        print("Failed to delete: " + filename + str(e))
        status = False
    return status


def getLength(path):
    """ Get the length of a file.

        Intended for getting the length of train moves - will
        probably make it more comprehensive later.
    """
    df, alert_list, warning_list = csvToDF(path)
    length_lines = len(open(path).readlines())
    los_events = len(alert_list) + len(warning_list)
    time = df['Time'][0]
    endtime = df['Time'].iloc[-1]
    return length_lines, endtime - time, los_events


def checkFile(path):
    """ Check various file properties.

        - Does it exist?
        - Is it readable
        - Is it nonzero in size?
    """
    response = True
    try:
        statinfo = os.stat(path)
        if statinfo.st_size == 0:
            print("File has no data:" + path)
            response = False
    except Exception as e:
        print("Could not stat: " + path + str(e))
        response = False
    # Check if we can read it
    if os.access(path, os.R_OK) is False:
        print("Could not read: " + path)
        response = False
    return response


def csvToDF(inputpath):
    import pandas as pd
    df = pd.read_csv(inputpath, dtype=str, header=1)
    df.columns = ['Time', 'Rx', 'Tx', 'Rx Ref. Ratio', 'Threshold Event']
    # Take care of samples missing the microseconds
    df['Time'] = df['Time'].apply(lambda x: x[:-1] + '.0Z'
                                  if '.' not in x else x)
    # Turn them into pandas datestamps
    df['Time'] = df['Time'].apply(pd.to_datetime,
                                  format="%Y-%m-%dT%H:%M:%S.%fZ")
    df['Rx'] = df['Rx'].astype(float)
    df['Tx'] = df['Tx'].astype(float)
    df['Rx Ref. Ratio'] = df['Rx Ref. Ratio'].astype(float)
    alert_list = df.index[df['Threshold Event'] == 'Alert'].tolist()
    warning_list = df.index[df['Threshold Event'] == 'Warning'].tolist()
    return df, alert_list, warning_list


def plotCSV(inputpath, title):
    import matplotlib
    import matplotlib.pyplot as plt
    df, alert_list, warning_list = csvToDF(inputpath)
    filebuffer = io.BytesIO()  # Make an in-memory file buffer for plot
    plt.rcParams["axes.titlesize"] = 8
    df = df.rename(columns={"Rx Ref. Ratio": "Rx Reference"})
    ax = df.plot.line(x='Time', y=['Rx', 'Tx', 'Rx Reference'])
    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M:%S"))
    ax.xaxis.set_major_locator(plt.MaxNLocator(10))
    ax.set_ylim(bottom=-0.5)
    for alert in alert_list:
        ax.axvline(x=df['Time'][alert], ymin=0, ymax=.05, linewidth=1, color='red')
    for warning in warning_list:
        ax.axvline(x=df['Time'][warning], ymin=0, ymax=.08, linewidth=1, color='gold')
    ax.set_title(title)
    ax.set_ylabel('Current (A)')
    ax.set_xlabel('Time')
    plt.savefig(filebuffer, dpi=300, pad_inches=0.4)
    filebuffer.seek(0)
    return filebuffer
