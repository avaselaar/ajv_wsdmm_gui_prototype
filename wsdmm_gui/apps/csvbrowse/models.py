###############################################################################
# Model Definition for the CSV browsing app
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.db import models
from django.utils import timezone


class ShuntCSV(models.Model):
    # Length of a CSV file in lines
    length_lines = models.IntegerField()
    # Track ID of the train move
    track_id = models.CharField(max_length=256)
    # Track ID of the train move
    slot_id = models.CharField(max_length=256)
    # Platform ID of the train move
    platform_id = models.CharField(max_length=256)
    # Full path to the file
    filename = models.CharField(max_length=256)
    # Just the basename of the file
    basename = models.CharField(max_length=256)
    # Status of LOS events in the file
    los_events = models.IntegerField()
    # Length of the train move itself
    train_length_timedelt = models.DurationField()
    # Timestamp of when the move occured
    train_timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.filename
