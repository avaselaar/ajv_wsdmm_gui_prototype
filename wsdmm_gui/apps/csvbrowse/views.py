###############################################################################
# CSV Browser for Django
#
# Ideally, this would be completely independent of shunt.  Alas, it is not.
#
# Maybe someday.
#
# Contact: andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from django.http import HttpResponse, HttpResponseRedirect, FileResponse
from django.urls import reverse
from django.views import generic
from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage

from common.wsdmmconf import wsdmmconf as wc

import io
from datetime import datetime
import mimetypes
import zipfile
from urllib.request import pathname2url

from .models import ShuntCSV
from . import utils

from django.apps import apps
SiteData = apps.get_model('main', 'SiteData')

# TODO Grab this from the database
path = '/data/outfiles'  # the path where NiFi outputs csv
wsdmm_conf = '/data/infiles/wsdmm.conf'


class indexView(generic.ListView):
    """ A view to display a list of available CSV Files
    """
    template_name = 'csvbrowse/list.html'
    context_object_name = 'csv_files'
    model = ShuntCSV

    def updateFiles(self):
        """ Sync path directory with the database list of available CSV files
        """
        ds = wc.wsdmmconf()
        if ds.loadConfig(wsdmm_conf) is True:
            data = ds.getConfig()
            site_data = SiteData(
                Division=data['Division'],
                SubDivision=data['SubDivision'],
                LineSegment=data['LineSegment'],
                Milepost=data['Milepost'],
                DOTNum=data['DOT#'],
                StreetName=data['StreetName'],
                City=data['City'],
                State=data['State'],
                Country=data['Country'],
            )
        else:
            print("Missing: " + wsdmm_conf)
            return

        train_moves = utils.enumerateFiles(path, site_data)

        # Add new files to db
        for move in train_moves:
            if ShuntCSV.objects.filter(filename=move).exists() is False:
                if utils.checkFile(move) is True:
                    temp_name = move.rsplit('/', 1)[1]
                    temp_name = temp_name.rsplit('.', 1)[0]
                    metadata = utils.parseName(move)
                    length_lines, train_length, los_events = utils.getLength(move)
                    newobject = ShuntCSV(
                        filename=move,
                        length_lines=length_lines,
                        basename=temp_name,
                        track_id=metadata['track'],
                        slot_id=metadata['slot'],
                        platform_id=metadata['platform_id'],
                        los_events=los_events,
                        train_length_timedelt=train_length,
                        train_timestamp=metadata['date'])
                    newobject.save()
                else:
                    print("Unable to add file: " + move)

        # Remove stale/unreadable files from db
        for fileobject in ShuntCSV.objects.order_by('-filename'):
            if fileobject.filename not in train_moves:
                fileobject.delete()
            else:
                if utils.checkFile(fileobject.filename) is False:
                    fileobject.delete()

    def get_ordering(self):
        self.updateFiles()
        ordering = self.request.GET.get('ordering', '-train_timestamp')
        # validate ordering here
        return ordering

    def render_to_response(self, context):
        ds = wc.wsdmmconf()
        if ds.loadConfig(wsdmm_conf) is False:
            print("Site not configured. Missing config file")
            return redirect('main:configerror')
        return super().render_to_response(context)


class detailView(generic.DetailView):
    """ A page that shows some statistics (and soon a plot) about a CSV.
    """
    model = ShuntCSV
    template_name = 'csvbrowse/detail.html'


def plotCheck(pk, location='url'):
    fs = FileSystemStorage()
    csv = ShuntCSV.objects.get(pk=pk)
    pngname = getPlotName(csv.basename)
    if fs.exists(pngname) is False:
        try:
            filebuffer = utils.plotCSV(csv.filename, pngname[:-9])
            print("Plotting: " + pngname)
            status = fs.save(pngname, filebuffer)
            print("Save Status:" + str(status))
        except Exception as e:
            print("Plotting failed: " + str(e))
            return '', ''
    if location == "url":
        image_location = fs.url(pngname)
    elif location == "path":
        image_location = fs.path(pngname)
    return image_location, pngname


def csvView(request, pk):
    csv = ShuntCSV.objects.get(pk=pk)
    image_url, _ = plotCheck(pk)
    context = {'basename': csv.basename,
               'length_lines': csv.length_lines,
               'platform_id': csv.platform_id,
               'slot_id': csv.slot_id,
               'track_id': csv.track_id,
               'pngname': image_url,
               'los_events': csv.los_events,
               'train_time': csv.train_length_timedelt,
               'id': csv.id,
               }

    return render(request, 'csvbrowse/detail_plot.html', context)


def downloadFile(request, pk):
    """ Download a CSV file to the browser, selected by its pk.
    """
    the_object = ShuntCSV.objects.get(pk=pk)
    fl = open(the_object.filename, 'r')
    mime_type, _ = mimetypes.guess_type(the_object.filename)
    response = HttpResponse(fl, content_type=mime_type)
    response['Content-Disposition'] = "attachment; filename=%s" % pathname2url(the_object.filename)
    return response


def deleteHelper(pk):
    fs = FileSystemStorage()
    query = ShuntCSV.objects.filter(id=pk)
    for csv in query:
        # Delete from the filesystem
        status = utils.deleteFile(csv.filename)
        pngname = getPlotName(csv.basename)
        # Remove from the Database
        if status is True:
            csv.delete()
        # Delete plot if there is one
        if fs.exists(pngname) is True:
            fs.delete(pngname)


def deleteFile(request, pk):
    """ Delete a file from the CSV folder and the database, and any
        plots
    """
    deleteHelper(pk)
    response = HttpResponseRedirect(reverse('csvbrowse:index'))
    return response


def getPlotName(basename):
    return basename + '_plot.png'


def chkBoxHandler(request):
    """ Do things with the checkboxes on the list page.

        Either download the checked items in a zip, or delete
        them.
    """
    if request.POST['button'] == 'download_all':
        csv_list = ShuntCSV.objects.all().values_list('id', flat=True)
        response = downloadMultiple(csv_list)
    elif request.POST['button'] == 'delete_all':
        csv_list = ShuntCSV.objects.all().values_list('id', flat=True)
        for csv in csv_list:
            deleteHelper(csv)
        response = HttpResponseRedirect(reverse('csvbrowse:index'))
    elif len(request.POST.getlist('pk')) > 0:
        if request.POST['button'] == 'download':
            response = downloadMultiple(request.POST.getlist('pk'))
        if request.POST['button'] == 'delete':
            for item in request.POST.getlist('pk'):
                deleteHelper(item)
            response = HttpResponseRedirect(reverse('csvbrowse:index'))
    else:
        response = HttpResponseRedirect(reverse('csvbrowse:index'))
    return response


def downloadMultiple(download_list):
    """ Load up CSV files into a zip file, provide it as a download.

        Does not write to the hard disk - works in memory.
    """
    zipbuffer = io.BytesIO()  # Make an in-memory file buffer for zipfile
    now = datetime.now()    # Generate a timestamp
    name = now.strftime("%Y%m%d_%H%M%S") + "_shunt.zip"  # Generate filename
    try:  # Make a zipfile
        zf = zipfile.ZipFile(zipbuffer, mode='w',
                             compression=zipfile.ZIP_DEFLATED)
        for item in download_list:
            the_object = ShuntCSV.objects.get(pk=item)
            image_path, plot_filename = plotCheck(item, location='path')
            print("Zipping... " + the_object.filename)
            zf.write(the_object.filename, arcname=the_object.basename + ".csv")
            print("Zipping... " + plot_filename)
            zf.write(image_path, arcname=plot_filename)
        zf.close()
        zipbuffer.seek(0)
        response = FileResponse(zipbuffer, as_attachment=True, filename=name)
    except Exception as e:
        print("Unable to create zip file" + name + " " + str(e))
        response = HttpResponseRedirect(reverse('csvbrowse:index'))
    return response
