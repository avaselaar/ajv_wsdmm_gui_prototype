#!/usr/bin/env python3
###############################################################################
# This module manages the wsdmm configuration file, both at the command line
# and via an API for django.  It contains no django-specific code.
#
# Contact:  andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
import json


class wsdmmconf:

    def __init__(self):
        self.config = {}
        pass

    def loadConfig(self, filename):
        """ Opens a WSDMM configuration file, saves it to the class variable

            Returns:
            0 - success
            1 - Fail
        """
        self.config = {}
        try:
            with open(filename, 'r') as config_file:
                self.config = json.load(config_file)
        except Exception as e:
            print("Unable to Load " + filename + " : " + str(e))
            return False
        return True

    def writeConfig(self, filename):
        """ Writes the currently loaded config to a file

            Returns:
            0 - success
            1 - Fail
        """
        try:
            with open(filename, 'w') as config_file:
                config_file.write(json.dumps(self.config, sort_keys=True, indent=4))
        except Exception as e:
            print("Unable to Write " + filename + " : " + str(e))
            return False
        return True

    def setConfig(self, config):
        """ Takes a configuration json string, saves it to the class variable

            Returns:
            0 - success
            1 - Fail
        """
        self.config['device_settings']['geographic_nfo'][0] = config
        return True

    def getConfig(self):
        """ Returns the currently loaded configuration file as a JSON string

        """
        return self.config['device_settings']['geographic_nfo'][0]

    def printConfig(self):
        """ Pretty prints the loaded config to console
        """
        print(json.dumps(self.config, sort_keys=True, indent=4))


def main():
    wc = wsdmmconf()
    wc.loadConfig('wsdmm.conf')
    wc.printConfig()
    wc.writeConfig('wsdmm_out.conf')


if __name__ == "__main__":
    main()
