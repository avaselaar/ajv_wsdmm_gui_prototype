#!/usr/bin/env python3
###############################################################################
# This module provides utility functions for talking to wsdmm api
#
# Contact:  andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
import functools
import requests
import re
import logging
import json
from django.http import HttpResponse
from apps.main.models import wsdmmService

logging.basicConfig(level=logging.DEBUG)

default_service_hosts = {
    'vti': {
        'hostname': 'nifi',
        'port': 8090,
    },
    'shunt': {
        'hostname': 'nifi',
        'port': 8090,
    },
    'tcm': {
        'hostname': 'nifi',
        'port': 8090,
    },
    'main': {
        'hostname': 'nifi',
        'port': 8090,
    },
    'influx': {
        'hostname': 'influx',
        'port': 8086,
    }
}


class wsdmmapi:
    def __init__(self, service):
        self.service = service

    def initWSDMMAPI(self):
        # NEed to comment out and pass this function before running migrations or migrations will fail
        # We'll give it a sane default:
        api_service = wsdmmService.objects.filter(serviceName='wsdmm_api')
        if not api_service:
            logging.info("No server configured, using sane default.")
            defaulthost = wsdmmService(
                hostname='wsdmm_api',
                hostport=8001,
                serviceName='wsdmm_api',
            )
            defaulthost.save()
            api_service = wsdmmService.objects.filter(serviceName='wsdmm_api')
        self.wsdmm_api_path = 'http://' + api_service[0].hostname + ':' + str(api_service[0].hostport) + '/api/v1/'
        # pass

        # TODO Check to see if the connection info configured in django differs from that which the API is using - aka, detect a change
        # in servers
    def check(self, func):
        @functools.wraps(func)
        def check_func(*args, **kwargs):
            self.initWSDMMAPI()  # Make sure API connection is available
            r = requests.get(self.wsdmm_api_path + 'status/services')  # Get list of services from API
            if self.service in r.json():  # If its there, proceed with the function
                return func(*args, **kwargs)
            else:
                if not self.initServices():  # Otherwise, try to initiate it
                    return func(*args, **kwargs)
                else:
                    return HttpResponse(self.service + ' NOT available')
        return check_func

    def initServices(self):
        servlist_count = wsdmmService.objects.filter(serviceName=self.service).count()
        service_data = []
        if servlist_count == 0:  # Service uninitialized, try a default
            defaulthost = wsdmmService(
                hostname=default_service_hosts[self.service]['hostname'],
                hostport=default_service_hosts[self.service]['port'],
                serviceName=self.service,
            )
            defaulthost.save()
        servlist = wsdmmService.objects.filter(serviceName=self.service)  # Get services fron django db
        for ssd in servlist:
            service_data.append({'service': ssd.serviceName, 'hostname': ssd.hostname, 'port': ssd.hostport})
        r = requests.post(self.wsdmm_api_path + 'init/services', json={'service': service_data})
        logging.debug("%s", r)
        if r.status_code == 200:
            logging.debug("Initialized API server")
            return 0
        else:
            return 1

    def pURLName(self, processor_name):
        if len(processor_name) == 3 and re.match('[1-9]_[1-2]', processor_name):
            url = 'conf/' + self.service + '/track/' + processor_name
            pdispname = processor_name
        elif 'mqtt' in processor_name:
            url_path = processor_name.replace('_', '/')
            url = 'conf/' + self.service + '/' + url_path
            pdispname = 'MQTT ' + processor_name.upper()  # Display Name
        return url, pdispname

    def proctoName(self, item):
        if re.match('[1-9]_[1-2]', item):
            return item
        if re.match('^mqtt', item):
            return item.replace('_', ' - ').upper()

    def getTrackIDs(self, service):
        # Get list of tracks
        r = self.get('status/' + service + '/tracks')
        if r.status_code == 200:
            track_list = json.loads(r.json())
        else:
            return -1

        # Cut down to track Ids
        # TODO turn this into a list comprehension
        track_ids = []
        for each in track_list:
            temp = each.split('_')
            track_ids.append(temp[1] + '_' + temp[2])
        return track_ids

    def get(self, url_stub):
        print(self.wsdmm_api_path)
        return requests.get(self.wsdmm_api_path + url_stub)

    def postJSON(self, url_stub, data):
        return requests.post(self.wsdmm_api_path + url_stub, json=data)
