#!/usr/bin/env python3
from influxdb import DataFrameClient, InfluxDBClient
import logging

logging.basicConfig(level=logging.DEBUG)


class wsdmminflux:
    """ A class for managing a WSDMM Influx Instance.

        The other data to be specified at initialization are the hostname and
        port of the influx API.
    """
    def __init__(self, hostname='influx', port=8086):
        self.setHost(hostname, port)

    def setHost(self, hostname, port):
        """
        Sets the hostname and port of the NiFi instance.
        """
        self.hostname = hostname
        self.port = port

    def getQuery(self, db, query, time_period, group_by='5m'):
        idb = InfluxDBClient(host=self.hostname, port=self.port)
        logging.debug("QUERY: " + str.format(query, time_period, group_by))
        return idb.query(str.format(query, time_period, group_by), epoch='s')

    def getQueryDF(self, db, query, time_period, group_by):
        idb = DataFrameClient(host=self.hostname, port=self.port, database=db)
        logging.debug("QUERY: " + str.format(query, time_period, group_by))
        return idb.query(str.format(query, time_period, group_by))
