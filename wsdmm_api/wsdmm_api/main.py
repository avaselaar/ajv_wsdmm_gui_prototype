#!/usr/bin/env python3
###############################################################################
# This module is a FastAPI server for the WSDMM, the goal of which is to
# standardize a WSDMM API for dealing with services on the WSDMM.
# It uses nipyapi to do the actual communication with NiFi, while
# all of the flow-specific knowledge lives here.
#
# Contact:  andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT


# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
from fastapi import FastAPI, Response
from pydantic import BaseModel, Field
from typing import List, Optional
import functools
import logging
import re
import json
import datetime

from .wsdmmnifi import wsdmmnifi
from .wsdmminflux import wsdmminflux

wsdmm_api = FastAPI()
logger = logging.getLogger(__name__)


class srv:
    services = {}


api_root = "/api/v1/"  # Root of the URL

# NiFi Constants###############################################################
tcm_pg_name = 'VTI_data_for_track_health_margin'
shunt_pg_name = 'VTI_data_for_train_moves_process_group'

# Because they're named differently
track_id_field_names = {
    tcm_pg_name: 'Track ID',
    shunt_pg_name: 'TRACK_ID',
}

# One place to change!
track_processor_stubs = {
    tcm_pg_name: 'TrackHealthMargin_Instance',
    shunt_pg_name: 'DetectTrainMoves_Instance',
}

# Influx Constants
queries = {
    'telegraf': {
        "meanCurrent": ('last', 'SELECT "meanCurrent" FROM "telegraf_ret_pol"."mqtt_consumer" WHERE {}'),
        "operating_margin_max": ('last', 'SELECT last("max") FROM "telegraf_ret_pol"."mqtt_consumer" WHERE {}'),
        "operating_margin_min": ('last', 'SELECT last("min") FROM "telegraf_ret_pol"."mqtt_consumer" WHERE {}'),
        "blowby_threshold": ('last', 'SELECT last("blowby") FROM "telegraf_ret_pol"."mqtt_consumer" WHERE {} GROUP BY time({}) fill(none)'),
        "dropout_threshold": ('last', 'SELECT last("dropout") FROM "telegraf_ret_pol"."mqtt_consumer"  WHERE {} GROUP BY time({}) fill(none)'),
    },
}


###############################################################################
#                              Data Models                                    #
###############################################################################
class Service(BaseModel):
    service: str
    hostname: str
    port: int


class Services(BaseModel):
    service: List[Service] = None


class PublishMQTTConfig(BaseModel):
    broker_uri: str = Field(None,
                            alias='Broker URI',
                            title='Broker URI',
                            description="The URI to use to connect to the MQTT broker (e.g. tcp://localhost:1883). The 'tcp', 'ssl', 'ws' and 'wss' schemes are supported. In order to use 'ssl', the SSL Context Service property must be set.")
    client_id: str = Field(None,
                           alias='Client ID',
                           title='Client ID',
                           description="MQTT client ID to use")
    username: Optional[str] = Field(None,
                                    alias='Username',
                                    title='Username',
                                    description="Username to use when connecting to the broker")
    password: Optional[str] = Field(None,
                                    alias='Password',
                                    title='Password',
                                    description="Password to use when connecting to the broker")
    ssl_context_service: Optional[str] = Field(None,
                                               alias='SSL Context Service',
                                               title='SSL Context Service',
                                               description="The SSL Context Service used to provide client certificate information for TLS/SSL connections.")
    last_will_topic: Optional[str] = Field(None,
                                           alias='Last Will Topic',
                                           title='Last Will Topic',
                                           description="The topic to send the client's Last Will to. If the Last Will topic and message are not set then a Last Will will not be sent.")
    last_will_message: Optional[str] = Field(None,
                                             alias='Last Will Message',
                                             title='Last Will Message',
                                             description="The message to send as the client's Last Will. If the Last Will topic and message are not set then a Last Will will not be sent.")
    last_will_retain: Optional[str] = Field(None,
                                            alias='Last Will Retain',
                                            title='Last Will Retain',
                                            description="Whether to retain the client's Last Will. If the Last Will topic and message are not set then a Last Will will not be sent.")
    last_will_qos_level: Optional[int] = Field(None,
                                               alias='Last Will QoS Level',
                                               title='Last Will QoS Level',
                                               description="QoS level to be used when publishing the Last Will Message")
    session_state: str = Field(None,
                               alias='Session State',
                               title='Session State',
                               description="Whether to start afresh or resume previous flows. See the allowable value descriptions for more details.")
    mqtt_spec_version: str = Field(None,
                                   alias='MQTT Specification Version',
                                   title='MQTT Specification Version',
                                   description="The MQTT specification version when connecting with the broker. See the allowable value descriptions for more details.")
    connection_timeout: Optional[int] = Field(None,
                                              alias='Connection Timeout (seconds)',
                                              title='Connection Timeout (seconds)',
                                              description="Maximum time interval the client will wait for the network connection to the MQTT server to be established. The default timeout is 30 seconds. A value of 0 disables timeout processing meaning the client will wait until the network connection is made successfully or fails.")
    keep_alive_interval: Optional[int] = Field(None,
                                               alias='Keep Alive Interval (seconds)',
                                               title='Keep Alive Interval (seconds)',
                                               description="Defines the maximum time interval between messages sent or received. It enables the client to detect if the server is no longer available, without having to wait for the TCP/IP timeout. The client will ensure that at least one message travels across the network within each keep alive period. In the absence of a data-related message during the time period, the client sends a very small 'ping' message, which the server will acknowledge. A value of 0 disables keepalive processing in the client.")
    topic: str = Field(None,
                       alias='Topic',
                       title='Topic',
                       description="The topic to publish the message to.")
    qos: int = Field(None,
                     alias='Quality of Service(QoS)',
                     title='Quality of Service(QoS)',
                     description="The Quality of Service(QoS) to send the message with. Accepts three values '0', '1' and '2'; '0' for 'at most once', '1' for 'at least once', '2' for 'exactly once'. Expression language is allowed in order to support publishing messages with different QoS but the end value of the property must be either '0', '1' or '2'. ")
    retain_message: bool = Field(None,
                                 alias='Retain Message',
                                 title='Retain Message',
                                 description="Whether or not the retain flag should be set on the MQTT message.")


class VTIConfig(BaseModel):
    chassis_id: str = Field(None,
                            alias='ElectrologIXS Chassis ID',
                            title='ElectrologIXS Chassis ID',
                            description="Human-readable description for ElectrologIXS Chassis ID.")
    hostname: str = Field(None,
                          alias='ElectrologIXS Hostname',
                          title='ElectrologIXS Hostname',
                          description="Hostname or IP address of ElectrologIXS device the VTI data is coming from.")
    eixs_udp_port: int = Field(None,
                               alias='ElectrologIXS UDP Port',
                               title='ElectrologIXS UDP Port',
                               description="ElectrologIXS UDP port for VTI data.")
    local_udp_port: int = Field(None,
                                alias='Local UDP Port',
                                title='Local UDP Port',
                                description="Local UDP port to send/receive VTI traffic to/from.")
    slots: str = Field(None,
                       alias='Module Slots',
                       title='Module Slots',
                       description="A comma-separated list of module slot numbers (1 - 9).  The list must contain at least one slot number and at most four slot numbers.  Duplicate slot numbers will be filtered out")
    site_id_req_to: int = Field(None,
                                alias='Site ID Request Timeout (ms)',
                                title='Site ID Request Timeout (ms)',
                                description="On processor startup, a request is sent to the ElectrologIXS device requesting the Site ID.  If a Site ID response is not received within the specified timeout period, then the Site ID is re-requested, and will keep being re-requested until a proper response is received within the timeout period, or until the processor is stopped, or the NiFi/MiNiFi instance is shutdown.  VTI data is not collected until a valid Site ID is received.")
    vti_data_timeout: int = Field(None,
                                  alias='VTI Data Timeout (ms)',
                                  title='VTI Data Timeout (ms)',
                                  description="After the Site ID is successfully received, a message is sent to the ElectrologIXS device to turn on the VTI data streaming of the of the ElectrologIXS.  This streaming data is captured and pushed out as the flowfile output of this processor.  If no streaming data is received within the specified timeout, a message is sent to the ElectrologIXS stop streaming, the Site ID is re-requested, and VTI data is re-requested.")


class DetectTrainMovesConfig(BaseModel):
    track_num: int = Field(None,
                           alias='Track Number',
                           title='Track Number',
                           description="The track number on the VTI to watch.")
    slot_num: int = Field(None,
                          alias='Slot Number',
                          title='Slot Number',
                          description="The slot number of the VTI card that has the track of interest.")
    time_margin: Optional[int] = Field(None,
                                       alias='TRAIN_MOVE_TIME_MARGIN',
                                       title='Time Margin (sec)',
                                       description="Specifies the leading and trailing time margin, in seconds, to include with a detected train move.")
    rx_alert_threshold: Optional[int] = Field(None,
                                              alias='RX_ALERT_THRESHOLD_IN_MA',
                                              title='Rx Alert Threshold (mA)',
                                              description="Specifies the alerting threshold for Rx data. Rx samples in margin: N/A. Rx samples in train move where value is 0: None. Rx samples in train move where value is (0, Rx Alert Threshold): Warning. Rx samples in train move where value is [Rx Alert Threshold, infinity): Alert.")
    rx_sample_data_dt: Optional[int] = Field(None,
                                             alias='RX_ALERT_DELTA_T_IN_SEC',
                                             title='Rx Sample Data Δt (sec)',
                                             description="The amount of Rx sample data to ignore (in seconds) from the end of a train when checking the train move for loss of shunt events.")


class TrackHealthMarginConfig(BaseModel):
    track_num: int = Field(None,
                           alias='Track Number',
                           title='Track Number',
                           description="The track number on the VTI to watch.")
    slot_num: int = Field(None,
                          alias='Slot Number',
                          title='Slot Number',
                          description="The slot number of the VTI card that has the track of interest.")
    rx_sample_data_dt: str = Field(None,
                                   alias='Rx Sample Data Δt (sec)',
                                   title='Rx Sample Data Δt (sec)',
                                   description="The amount of Rx sample data to ignore (in seconds) from the beginning of a train move when checking for track circuit margin alerts.")
    sample_window_size: str = Field(None,
                                    alias='Sample Windows Size',
                                    title='Sample Windows Size',
                                    description="Sample window size, in duration, of how much data should be sampled and summarized.  Must be of format <duration> <TimeUnit> where <duration> is a non-negative integer and TimeUnit is a supported Time Unit, such as: nanos, millis, secs, mins, hrs, days")
    operating_margin_window_size: str = Field(None,
                                              alias='Operating Margin Window Size',
                                              title='Operating Margin Window Size',
                                              description="The operating margin is a sliding window of time where the min and max signal values are used to determine the operating margin.  This configuration defines the width of this sliding window in time.  Default is a 5 day wide sliding window.  Must be of format <duration> <TimeUnit> where <duration> is a non-negative integer and TimeUnit is a supported Time Unit, such as: nanos, millis, secs, mins, hrs, days")
    report_interval: str = Field(None,
                                 alias='Report Interval',
                                 title='Report Interval',
                                 description="Amount of time worth of data to analyze and generate a report on.  Must be of format <duration> <TimeUnit> where <duration> is a non-negative integer and TimeUnit is a supported Time Unit, such as: nanos, millis, secs, mins, hrs, days")
    reliability_margin: float = Field(None,
                                      alias='Reliability Margin (%)',
                                      title='Reliability Margin (%)',
                                      description="If the max signal value approaches the blow-by threshold pushing the reliability margin below this percentage, an excursion event is generated.")
    safety_margin: float = Field(None,
                                 alias='Safety Margin (%)',
                                 title='Safety Margin (%)',
                                 description="If the min signal value approaches the drop-out threshold pushing the safety marginb below this percentage, an excursion event is generated.")
    blow_by_threshold: float = Field(None,
                                     alias='Blow-by Threshold (amps)',
                                     title='Blow-by Threshold (amps)',
                                     description="The upper bound on Rx current in milliamps.")
    drop_out_threshold: float = Field(None,
                                      alias='Drop-out Threshold (amps)',
                                      title='Drop-out Threshold (amps)',
                                      description="The lower bound on Rx current in milliamps.")
# End Data Models###########################################################


# Root Endpoint#################################################################
@wsdmm_api.get("/")
async def root():
    return {"message": "WSDMM API v0.0.0.0.1pre-alpha"}


###############################################################################
#                              Server Management                              #
###############################################################################
# Endpoints#################################################################
@wsdmm_api.post(api_root + "init/services")
async def serviceInit(services: Services):
    """
    Takes a json structure that lists each of the servers to connecto and the
    service they provide. Note that one actual server (ie, nifi), can provide
    more than one service.

    The idea here is that if in the future we move something out of Nifi,
    we can just update this code and the appropriate server will be connected to.

    It also opens up the possibility for a service that requires initializing
    multiple server connections.
    """
    for ser in services.service:
        if ser.service == 'vti':
            nifiInit(ser.hostname, ser.port, ser.service)
        if ser.service == 'tcm':
            nifiInit(ser.hostname, ser.port, ser.service)
        if ser.service == 'shunt':
            nifiInit(ser.hostname, ser.port, ser.service)
        if ser.service == 'sear':
            nifiInit(ser.hostname, ser.port, ser.service)
        if ser.service == 'reporter':
            nifiInit(ser.hostname, ser.port, ser.service)
        if ser.service == 'influx':
            influxInit(ser.hostname, ser.port, ser.service)
    return srv.services


@wsdmm_api.get(api_root + "status/services")
async def servicesStatus():
    return srv.services


@wsdmm_api.get(api_root + "status/service/{service}")
async def serviceStatus(service: str):
    if service in srv.services:
        data = srv.services[service]
        logger.debug('Service %s initialized', service)
        return {
            'hostname': data.hostname,
            'hostport': data.port,
            'service': data.service,
            'status': getServiceState(service)
        }
    else:
        logger.debug('Service %s not initialized', service)
        return {
            'hostname': None,
            'hostport': None,
            'server': None,
            'status': None,
        }


# Helper Functions##############################################################
def getServiceState(service):
    if service == 'shunt':
        return srv.nifi.getStatus(shunt_pg_name)
    if service == 'tcm':
        return srv.nifi.getStatus(tcm_pg_name)
    if service == 'influx':
        return 'OK, probably'


def nifiInit(hostname, port, service):
    print(hostname)
    if hasattr(srv, 'nifi') is not True:
        logger.info("Connecting to nifi at %s:%d for service %s", hostname, port, service)
        try:
            srv.nifi = wsdmmnifi.wsdmmnifi(hostname, port)
        except Exception as e:
            logger.info("Nifi Init Failed: " + str(e))
    record = Service(
        service=service,
        hostname=hostname,
        port=port,
        status=getServiceState(service)
    )
    srv.services[service] = record
    return 0


def influxInit(hostname, port, service):
    if hasattr(srv, 'influx') is not True:
        logger.info("Connecting to influx at %s:%d for service %s", hostname, port, service)
        srv.influx = wsdmminflux.wsdmminflux(hostname, port)
    record = Service(
        service=service,
        hostname=hostname,
        port=port,
        status=getServiceState(service)
    )
    srv.services[service] = record
    return 0


# Server Decorators#############################################################
def serviceCheck(service):
    """
    This decorator only runs the function if the service has been configured.
    """
    def actual_decorator(func):
        @functools.wraps(func)
        def service_func(*args, **kwargs):
            if service in srv.services:
                return func(*args, **kwargs)
            else:
                logger.error(str(service) + " not initialized")

        return service_func
    return actual_decorator
# End Server Management#########################################################


###############################################################################
#                                VTI Management                               #
###############################################################################
# Endpoints#####################################################################
@serviceCheck('vti')
@wsdmm_api.get(api_root + "conf/hw/vti2s")
async def getVTI(schema: bool = False):
    """
    Returns configuration file data for the VTI Card, retrieved from whereever it is
    configured. Nifi, in this case.
    """
    data = srv.nifi.getProcessor('GetVTIData')
    model = nifiToModel(data, VTIConfig())
    return addValuetoSchema(model)


@serviceCheck('vti')
@wsdmm_api.post(api_root + "conf/hw/vti2s")
async def putVTI(vticonfig: VTIConfig):
    """
    Sets the configuration for the VTI Card to wherever it should go
    """
    processor = srv.nifi.getProcessor('GetVTIData')
    processor = modelToNifi(processor, vticonfig)
    status = srv.nifi.updateGetVTIData(processor)
    return status
# End VTI Management############################################################


###############################################################################
#                                 Shunt Health                                #
###############################################################################
# Endpoints#####################################################################
@serviceCheck('shunt')
@wsdmm_api.get(api_root + "conf/shunt")
async def getShuntConfig():
    """
        Returns a list of configurable processors in shunt.
    """
    data = srv.nifi.getGroupEditableKeys(shunt_pg_name)
    outdata = {}
    for key in data.items():
        if re.match('^Detect.*', key[0]):
            temp = data[key[0]][track_id_field_names[shunt_pg_name]]['value'].split('_')
            track_id = str(temp[1]) + '_' + str(temp[2])
            outdata[track_id] = {'name': track_id}
        elif re.match('TrainMoveReportCsv.*', key[0]):
            outdata['mqtt_csv'] = {'name': "MQTT - CSV"}
        elif re.match('TrainMoveReportJson.*', key[0]):
            outdata['mqtt_json'] = {'name': "MQTT - JSON"}
    return outdata


@serviceCheck('shunt')
@wsdmm_api.get(api_root + "conf/shunt/track/{track}")
async def getShuntTrackConfig(track):
    """
    Returns the properties for a single track processor.
    """
    track_config = getTrackConfig(shunt_pg_name, track, track_processor_stubs[shunt_pg_name], DetectTrainMovesConfig())
    return addValuetoSchema(track_config)


@serviceCheck('shunt')
@wsdmm_api.post(api_root + "conf/shunt/track/{track}")
async def postShuntTrackConfig(track, dtmi_config: DetectTrainMovesConfig):
    """
    Allows setting the editable config properties for shunt
    """
    return updateTrack(track, track_processor_stubs[shunt_pg_name], shunt_pg_name, dtmi_config)


@serviceCheck('shunt')
@wsdmm_api.get(api_root + "status/shunt")
async def getShuntStatus():
    return srv.nifi.getServiceState('shunt')


@serviceCheck('shunt')
@wsdmm_api.get(api_root + "sched/shunt")
async def scheduleShunt(desired_state: bool):
    srv.nifi.scheduleService('shunt', desired_state)


@serviceCheck('shunt')
@wsdmm_api.get(api_root + "conf/shunt/mqtt/{endpoint}")
async def getShuntConfigMQTT(endpoint):
    if endpoint == 'mqtt_csv':
        mqtt_config = getMQTTConfig('TrainMoveReportCsvLocation_PublishMQTT', shunt_pg_name)
        return addValuetoSchema(mqtt_config)
    elif endpoint == 'mqtt_json':
        mqtt_config = getMQTTConfig('TrainMoveReportJson_PublishMQTT', shunt_pg_name)
        return addValuetoSchema(mqtt_config)
    else:
        return {'message': 'Invalid Shunt MQTT endpoint'}


@serviceCheck('shunt')
@wsdmm_api.post(api_root + "conf/shunt/mqtt/{endpoint}")
async def postShuntConfigMQTT(endpoint, config: PublishMQTTConfig):
    if endpoint == 'mqtt_csv':
        processor = srv.nifi.getProcessorInGroup('TrainMoveReportCsvLocation_PublishMQTT', shunt_pg_name)
    elif endpoint == 'mqtt_json':
        processor = srv.nifi.getProcessorInGroup('TrainMoveReportJson_PublishMQTT', shunt_pg_name)
    else:
        return {'message': 'Invalid Shunt MQTT endpoint'}
    processor = modelToNifi(processor, config)
    return srv.nifi.updateServiceProcessor(processor, 'shunt')


@serviceCheck('shunt')
@wsdmm_api.get(api_root + "status/shunt/tracks")
async def getShuntTrackStatus():
    return getTrackNames(shunt_pg_name, track_processor_stubs[shunt_pg_name])
# End Shunt Health##############################################################


###############################################################################
#                                 Track Health                                #
###############################################################################
# Endpoints#####################################################################
@serviceCheck('tcm')
@wsdmm_api.get(api_root + "conf/tcm")
async def getTCMConfig():
    """
    Returns a list of configurable processors for TCM
    """
    data = srv.nifi.getGroupEditableKeys(tcm_pg_name)
    outdata = {}
    for key in data.items():
        if re.match('^TrackHealthMargin_Instance.', key[0]):
            temp = data[key[0]][track_id_field_names[tcm_pg_name]]['value'].split('_')
            track_id = str(temp[1]) + '_' + str(temp[2])
            outdata[track_id] = {'name': track_id}
        elif key[0] == 'TrackHealthMarginHeartBeat_PublishMQTT':
            outdata['mqtt_json'] = {'name': "MQTT - JSON"}
    return outdata


@serviceCheck('tcm')
@wsdmm_api.get(api_root + "conf/tcm/track/{track}")
async def getTCMTrackConfig(track):
    """
    Returns the properties for a single track processor.
    """
    track_config = getTrackConfig(tcm_pg_name, track, track_processor_stubs[tcm_pg_name], TrackHealthMarginConfig())
    return addValuetoSchema(track_config)


@serviceCheck('tcm')
@wsdmm_api.post(api_root + "conf/tcm/track/{track}")
async def postTCMTrackConfig(track, thmi_config: TrackHealthMarginConfig):
    """
    Allows setting the editable config properties for tcm
    """
    return updateTrack(track, track_processor_stubs[tcm_pg_name], tcm_pg_name, thmi_config)


@serviceCheck('tcm')
@wsdmm_api.get(api_root + "status/tcm")
async def getTCMStatus():
    return srv.nifi.getServiceState('tcm')


@serviceCheck('tcm')
@wsdmm_api.get(api_root + "sched/tcm")
async def scheduleTCM(desired_state: bool):
    srv.nifi.scheduleService('tcm', desired_state)


@serviceCheck('tcm')
@wsdmm_api.get(api_root + "conf/tcm/mqtt/{endpoint}")
async def getTCMConfigMQTT(endpoint):
    if endpoint == 'mqtt_json':
        mqtt_config = getMQTTConfig('TrackHealthMarginHeartBeat_PublishMQTT', tcm_pg_name)
        return addValuetoSchema(mqtt_config)
    else:
        return {'message': 'Invalid TCM MQTT endpoint'}


@serviceCheck('tcm')
@wsdmm_api.post(api_root + "conf/tcm/mqtt/{endpoint}")
async def postTCMConfigMQTT(endpoint, config: PublishMQTTConfig):
    if endpoint == 'json':
        processor = srv.nifi.getProcessorInGroup('TrackHealthMarginHeartBeat_PublishMQTT', tcm_pg_name)
    else:
        return {'message': 'Invalid MQTT endpoint'}
    processor = modelToNifi(processor, config)
    return srv.nifi.updateServiceProcessor(processor, 'tcm')


@serviceCheck('tcm')
@wsdmm_api.get(api_root + "status/tcm/tracks")
async def getTCMTrackStatus():
    return getTrackNames(tcm_pg_name, track_processor_stubs[tcm_pg_name])
# End Track Health##############################################################


###############################################################################
#                               Influx Database                               #
###############################################################################
# Endpoints#####################################################################
@serviceCheck('influx')
@wsdmm_api.get(api_root + "data/tcm")
async def getTCMData(startTime: float, endTime: float, track: str, response: Response):
    query_start_time = datetime.datetime.fromtimestamp(startTime).isoformat()
    query_end_time = datetime.datetime.fromtimestamp(endTime).isoformat()
    track_config = getTrackConfig(tcm_pg_name, track, track_processor_stubs[tcm_pg_name], TrackHealthMarginConfig())
    if track_config != -1:
        vti_processor = srv.nifi.getProcessor('GetVTIData')
        chassis_id = vti_processor.component.config.properties['ElectrologIXS Chassis ID']
        track_id = chassis_id + '_' + str(track_config.slot_num) + '_' + str(track_config.track_num)
        site_id = 'AJV BasementA'
        tag = 'AND "tc_health"=\'tc/margin/v1/' + site_id + '/' + track_id + '/status\''
        results = srv.influx.getQueryDF('telegraf', queries['telegraf']['meanCurrent'][1], 'time >= \'' + query_start_time + '.000Z\' AND time <= \'' + query_end_time + '.000Z\' ' + tag, '1m')
        if 'mqtt_consumer' in results:
            temp = results['mqtt_consumer']
            tcmData = temp.rename(columns={'last': 'meanCurrent'})
            for queryname, query in queries['telegraf'].items():
                if queryname != 'meanCurrent':
                    new_results = srv.influx.getQueryDF('telegraf', query[1], 'time >= \'' + query_start_time + '.000Z\' AND time <= \'' + query_end_time + '.000Z\' ' + tag, '1m')
                    temp = new_results['mqtt_consumer']
                    tempData = temp.rename(columns={query[0]: queryname})
                    tcmData = tcmData.merge(tempData, how='outer', left_index=True, right_index=True)

            tcmData.fillna(method='bfill', inplace=True)
            tcmData.fillna(method='ffill', inplace=True)

            tcmData['reliability_margin'] = (tcmData['meanCurrent'].min() - tcmData['dropout_threshold']) / (tcmData['blowby_threshold'] - tcmData['dropout_threshold'] * 100)
            tcmData['safety_margin'] = (tcmData['blowby_threshold'] - tcmData['meanCurrent'].max()) / (tcmData['blowby_threshold'] - tcmData['dropout_threshold'] * 100)
            outdata = tcmData.to_json(date_format='epoch', orient='index')
            response.status_code = 200
        else:
            response.status_code = 204
            outdata = {'message': 'No Influx data found'}
        return outdata
    else:
        outdata = {'message': 'Track Not Found'}
        return outdata
# End Influx Database###########################################################


###########################################################################
#                         Global Helper Functions                         #
###########################################################################
def nifiToModel(processor, model):
    """
    This takes config data out of nifi processor config and puts it into a
    pydantic model. It uses the alias field defined in the model to match up to
    the nifi field name """
    data_dict = model.dict(by_alias=True)
    out_dict = data_dict.copy()
    for item in processor.component.config.properties:
        for key in data_dict.items():
            if key[0] == item:
                out_dict.update({key[0]: processor.component.config.properties[item]})
    model = model.parse_obj(out_dict)
    return model


def modelToNifi(processor, model):
    """
    This takes config data out of a model and puts it into a nifi processor config
    It uses the alias field defined in the model to match up to the nifi field
    name. """
    data_dict = model.dict(by_alias=True)
    for item in processor.component.config.properties:
        for key in data_dict.items():
            if key[0] == item:
                processor.component.config.properties[item] = data_dict[key[0]]
    return processor


def trackCheck(track_id):
    """
    Checks if an incoming track id is only 1 char long and is a - d.
    """
    if len(track_id) == 3 and re.match('[1-9]_[1-2]', track_id):
        return True
    else:
        logger.error('Invalid Track ID:' + track_id)
        return False


def findTrackProcessor(pg_name, track, processor_stub):
    if trackCheck(track):
        for i in range(1, 5):
            processor = srv.nifi.getProcessorInGroup(processor_stub + str(i), pg_name)
            candidate_id = processor.component.config.properties[track_id_field_names[pg_name]].split('_')
            if candidate_id[1] + '_' + candidate_id[2] == track:
                return processor
    logger.error('Track Not found: ' + track)
    return -1


def getTrackNames(pg_name, processor_stub):
    track_name_list = []
    for i in range(1, 5):
        processor = srv.nifi.getProcessorInGroup(processor_stub + str(i), pg_name)
        track_name_list.append(processor.component.config.properties[track_id_field_names[pg_name]])
    return json.dumps(track_name_list)


def getTrackConfig(pg_name, track, processor_stub, target_model):
    """
    Returns a filled-in DetectTrainmoves or TrackHealthMargin Model
    """
    processor = findTrackProcessor(pg_name, track, processor_stub)
    if processor != -1:
        track_id = processor.component.config.properties[track_id_field_names[pg_name]].split('_')
        model = nifiToModel(processor, target_model)
        model.track_num = track_id[2]
        model.slot_num = track_id[1]
        return model
    else:
        logger.error('Track not found:' + track)
        return -1


def getMQTTConfig(processor_name, pg):
    """
    Returns a filled out MQTT config model.
    """
    processor = srv.nifi.getProcessorInGroup(processor_name, pg)
    return nifiToModel(processor, PublishMQTTConfig())


def addValuetoSchema(model):
    """
    Adds the value from a model to a schema definition, because its convenient and I haven't figured out
    how to make pydantic do it for me.
    """
    schema = model.schema()
    model_dict = model.dict(by_alias=True)
    outdict = schema['properties']
    for item, value in model_dict.items():
        for key in schema['properties']:
            if item == key:
                outdict[item].update({'value': value})
    return outdict


def updateTrack(track, processor_stub, pg_name, track_config):
    """
    Updates a nifi procesor with an incoming track configuration (detect train moves or track health margin).
    Automatically puts together the trackID from the VTI module and the slot/track number.
    """
    if trackCheck(track):
        processor = findTrackProcessor(pg_name, track, processor_stub)
        processor = modelToNifi(processor, track_config)
        # Automatically fill out the Track ID based on VTI config
        vti_processor = srv.nifi.getProcessor('GetVTIData')
        chassis_id = vti_processor.component.config.properties['ElectrologIXS Chassis ID']
        track_id = chassis_id + '_' + str(track_config.slot_num) + '_' + str(track_config.track_num)
        processor.component.config.properties[track_id_field_names[pg_name]] = track_id
        if processor_stub == track_processor_stubs[pg_name]:
            service = 'tcm'
        else:
            service = 'shunt'
        return srv.nifi.updateServiceProcessor(processor, service)
    else:
        return {'message': 'Invalid track'}
