#!/usr/bin/env python3
###############################################################################
# This module is a python library for communicating with the WSDMM NiFi
# REST interface. It uses nipyapi to do the actual communication with NiFi, while
# all of the flow-specific knowledge lives here.
# Contact:  andrew.vaselaar@alstomgroup.com
#
# COPYRIGHT
# | All rights reserved (c) 2020 ALSTOM, USA
# |
# | This computer program may not be used, copied, distributed,
# | corrected, modified, translated, transmitted or assigned
# | without ALSTOM's prior written authorization.
###############################################################################
import nipyapi
import time
import re
import logging
import requests

logging.basicConfig(level=logging.DEBUG)

# This dict is a list of Nifi config options that we want to allow downstream consumers to
# be able to edit.  Top level is the name of the processor, its children are the names of
# the configuration variables
ALLOW_EDITS = {
    # A dict containing processor names and variables which we want to allow
    # the user to edit.  Variables submitted that aren't in this list will be
    # ignored.
    'GetVTIData': [
        'ElectrologIXS Chassis ID',
        'ElectrologIXS Hostname',
        'ElectrologIXS UDP Port',
        'Local UDP Port',
        'Module Slots',
        'Site ID Request Timeout (ms)',
        'VTI Data Timeout (ms)',
        'VTI Data Timeout (ms)'
    ],
    'VTIToJson': [
        'Include Raw Bytes',
    ],
    'DetectTrainMoves_Instance1': [
        'RX_ALERT_THRESHOLD_IN_MA',
        'TRACK_ID',
        'TRAIN_MOVE_TIME_MARGIN'
    ],
    'DetectTrainMoves_Instance2': [
        'RX_ALERT_THRESHOLD_IN_MA',
        'TRACK_ID',
        'TRAIN_MOVE_TIME_MARGIN'

    ],
    'DetectTrainMoves_Instance3': [
        'RX_ALERT_THRESHOLD_IN_MA',
        'TRACK_ID',
        'TRAIN_MOVE_TIME_MARGIN'
    ],
    'DetectTrainMoves_Instance4': [
        'RX_ALERT_THRESHOLD_IN_MA',
        'TRACK_ID',
        'TRAIN_MOVE_TIME_MARGIN'
    ],
    'TrainMoveReportCsvLocation_PublishMQTT': [
        "Broker URI", \
        "Client ID", \
        "Username", \
        "Password", \

        "SSL Context Service", \
        "Last Will Topic", \
        "Last Will Message", \
        "Last Will Retain", \
        "Last Will QoS Level", \
        "Session state", \
        "MQTT Specification Version", \
        "Connection Timeout (seconds)", \
        "Keep Alive Interval (seconds)", \
        "Topic", \
        "Quality of Service(QoS)", \
        "Retain Message",
    ],
    'TrainMoveReportJson_PublishMQTT': [
        "Broker URI", \
        "Client ID", \
        "Username", \
        "Password", \
        "SSL Context Service", \
        "Last Will Topic", \
        "Last Will Message", \
        "Last Will Retain", \
        "Last Will QoS Level", \
        "Session state", \
        "MQTT Specification Version", \
        "Connection Timeout (seconds)", \
        "Keep Alive Interval (seconds)", \
        "Topic", \
        "Quality of Service(QoS)", \
        "Retain Message",
    ],
    'TrackHealthMarginHeartBeat_PublishMQTT': [
        "Broker URI", \
        "Client ID", \
        "Username", \
        "Password", \

        "SSL Context Service", \
        "Last Will Topic", \
        "Last Will Message", \
        "Last Will Retain", \
        "Last Will QoS Level", \
        "Session state", \
        "MQTT Specification Version", \
        "Connection Timeout (seconds)", \
        "Keep Alive Interval (seconds)", \
        "Topic", \
        "Quality of Service(QoS)", \
        "Retain Message",
    ],
    'TrackHealthMargin_Instance1': [
        "Track ID", \
        "Rx Sample Data Δt (sec)", \
        "Sample Windows Size", \
        "Operating Margin Window Size", \
        "Report Interval", \
        "Reliability Margin (%)", \
        "Safety Margin (%)", \
        "Blow-by Threshold (mA)", \
        "Drop-out Threshold (mA)", \
    ],
    'TrackHealthMargin_Instance2': [
        "Track ID", \
        "Rx Sample Data Δt (sec)", \
        "Sample Windows Size", \
        "Operating Margin Window Size", \
        "Report Interval", \
        "Reliability Margin (%)", \
        "Safety Margin (%)", \
        "Blow-by Threshold (mA)", \
        "Drop-out Threshold (mA)", \
    ],
    'TrackHealthMargin_Instance3': [
        "Track ID", \
        "Rx Sample Data Δt (sec)", \
        "Sample Windows Size", \
        "Operating Margin Window Size", \
        "Report Interval", \
        "Reliability Margin (%)", \
        "Safety Margin (%)", \
        "Blow-by Threshold (mA)", \
        "Drop-out Threshold (mA)", \
    ],
    'TrackHealthMargin_Instance4': [
        "Track ID", \
        "Rx Sample Data Δt (sec)", \
        "Sample Windows Size", \
        "Operating Margin Window Size", \
        "Report Interval", \
        "Reliability Margin (%)", \
        "Safety Margin (%)", \
        "Blow-by Threshold (mA)", \
        "Drop-out Threshold (mA)", \
    ],
}


class wsdmmnifi:
    """ A class for managing a WSDMM NiFi Instance.

        The other data to be specified at initialization are the hostname and
        port of the NiFi API.
    """
    def __init__(self, hostname='localhost', port=8090):
        self.setHost(hostname, port)

    def setHost(self, hostname, port):
        """ Sets the hostname and port of the NiFi instance.
        """
        self.hostname = hostname
        self.port = port
        self.path = 'http://' + self.hostname + ':' + str(self.port) + '/nifi-api'
        nipyapi.config.nifi_config.host = self.path

    def getProcessorList(self):
        """ Find the list of all processors on the canvas.

            Typically this is run once and stored, since it shouldn't be
            changing at runtime.
        """
        processor_list = nipyapi.canvas.list_all_processors()
        return processor_list

    def getEditableProcessors(self):
        """
        Gets a list of processors that we will allow to be edited.
        """
        outlist = {}
        processors = self.getProcessorList()
        for processor in processors:
            if self.getProcessorEditableKeys(processor.id):
                outlist.update({processor.component.name: self.getDisplayName(processor.component.name)})
        return outlist

    def getGroupProcessorList(self, group_name):
        """
        Gets a list of processors in a process group by the group name.
        """
        pg = nipyapi.canvas.get_process_group(group_name, identifier_type='name', greedy=False)
        return nipyapi.canvas.list_all_processors(pg.component.id)

    def getProcessorInGroup(self, name, group_name):
        """
        Gets a list of processors in a process group by the group name.
        """
        processor_list = self.getGroupProcessorList(group_name)
        for processor in processor_list:
            if processor.component.name == name:
                return processor
        return None

    def getProcessor(self, name):
        """
        Smartly Returns list of processors with a given name
        """
        if name == 'GetVTIData':
            return self.getProcessorInGroup(name, 'Track Circuit Health')
        else:
            return nipyapi.canvas.get_processor(name, identifier_type='name')

    def getDisplayName(self, shortname):
        """Creates a human readable name.  May no longer be necessary."""
        temp = re.sub("([a-z])([A-Z])", r"\g<1> \g<2>", shortname)
        temp = re.sub("([0-9])", r" \g<1>", temp)
        temp = re.sub("(VTI)", r"\g<1> ", temp)
        temp = temp.replace('_', ' ')
        return temp

    def getProcessorEditableKeys(self, processor_id):
        """ Get the list of keys that are editable for the given processor id.
        """
        d = {}  # Editable Properties

        # Grab fresh processor data
        p = nipyapi.canvas.get_processor(processor_id,
                                         identifier_type='id')
        if p.component.config.properties != {}:
            # Make sure we want to allow editing these values:
            if p.component.name in ALLOW_EDITS.keys():
                # copy them into a new dict
                for key in p.component.config.properties:
                    d[p.component.name] = {}
                    for k, v in p.component.config.properties.items():
                        if k in ALLOW_EDITS[p.component.name]:
                            d[p.component.name][k] = {}
                            d[p.component.name][k].update({"value": v})
                            d[p.component.name][k].update({'desc': p.component.config.descriptors[k].description})
                            d[p.component.name][k].update({'disp': p.component.config.descriptors[k].display_name})
        return d

    def getGroupEditableKeys(self, pg_name):
        """
        Get the editable keys, display descriptions and data values for an entire process group.
        """
        config_values = {}
        processors = self.getGroupProcessorList(pg_name)
        for processor in processors:
            temp = self.getProcessorEditableKeys(processor.component.id)
            if len(temp.items()) > 0:
                config_values.update(temp)
        return config_values

    def enableProcessor(self, processor, state):
        """ Enable a NiFi Processor !"""
        p = nipyapi.canvas.get_processor(processor.id, identifier_type='id')
        headers = {"Content-Type": "application/json"}
        if state:
            data = '{"revision":{"version":' + str(p.revision.version) + '},"state": "STOPPED"}'
        else:
            data = '{"revision":{"version":' + str(p.revision.version) + '},"state": "DISABLED"}'
        requests.put(self.path + '/processors/' + p.id + '/run-status', data=data, headers=headers)

    def enableInputPorts(self, pg_id, state):
        """ Enable input ports on a nifi canvas """
        input_ports = nipyapi.canvas.list_all_input_ports(pg_id=pg_id, descendants=True)
        headers = {"Content-Type": "application/json"}
        for p in input_ports:
            if state:
                data = '{"revision":{"version":' + str(p.revision.version) + '},"state": "STOPPED"}'
                requests.put(self.path + '/input-ports/' + p.id + '/run-status', data=data, headers=headers)
            else:
                data = '{"revision":{"version":' + str(p.revision.version) + '},"state": "DISABLED"}'
                requests.put(self.path + '/input-ports/' + p.id + '/run-status', data=data, headers=headers)

    def enableOutputPorts(self, pg_id, state):
        """ Enable output ports on a nifi canvas """
        output_ports = nipyapi.canvas.list_all_output_ports(pg_id=pg_id, descendants=True)
        headers = {"Content-Type": "application/json"}
        for p in output_ports:
            if state:
                data = '{"revision":{"version":' + str(p.revision.version) + '},"state": "STOPPED"}'
                requests.put(self.path + '/output-ports/' + p.id + '/run-status', data=data, headers=headers)
            else:
                data = '{"revision":{"version":' + str(p.revision.version) + '},"state": "DISABLED"}'
                requests.put(self.path + '/output-ports/' + p.id + '/run-status', data=data, headers=headers)

    def updateProcessor(self, processor):
        """ Update processor configuration from another processor config.  ASsumes the processor is stopped.

        Checks ALLOW_EDITS to make sure keys are allowed to be changed.
        """
        # Grab existing processor data
        p = nipyapi.canvas.get_processor(processor.id, identifier_type='id')
        # Go through the keys in the processor, copy over new values if they're in the editable list
        for key, value in p.component.config.properties.items():
            logging.debug("New Value for " + key + " Value: " + str(processor.component.config.properties[key]))
            if key in ALLOW_EDITS[p.component.name]:
                p.component.config.properties.update({key: processor.component.config.properties[key]})
        try:
            # Submit new confguration to Nifi
            results = nipyapi.canvas.update_processor(p, p.component.config)
        except Exception as e:
            # ruh roh
            logging.error("Unable to submit changes: " + str(e))
        else:
            # check validation state
            if results.component.validation_status == "VALID":
                logging.info("Succeessfully reconfigured " + p.component.name)
            else:
                p = nipyapi.canvas.get_processor(processor.id, identifier_type='id')
                # Replace with old value
                p.component.config.properties.update({key: value})
                nipyapi.canvas.update_processor(p, p.component.config)
                logging.error("Configuration value not VALID - change reverted - " + p.component.name)

    def updateServiceProcessor(self, processor, service):
        """ Handles the start and stop of the appropriate process group when reconfiguring
        a member"""
        service_status = self.getServiceStatus(service)
        self.scheduleService(service, False)
        self.updateProcessor(processor)
        self.scheduleService(service, service_status)

    def scheduleProcessor(self, processor, state):
        """ Attempts to schedule (start/stop) a processor.
            Usually, this works.  Nifi can be finicky though.
        """
        status = False
        count = 0
        while True:
            try:
                # Start processor
                status = nipyapi.canvas.schedule_processor(processor, state,
                                                           refresh=True)
                time.sleep(2)
            except Exception as e:
                logging.error("Failed to set " + processor.component.name + " of id " + processor.component.id + " to state: " + str(state) + str(e))
            count = count + 1
            if status is True or count == 20:
                break
        return status

    def scheduleService(self, service, state):
        """ Stops or starts a given service """
        # Deal with the other Process Groups
        if service == 'shunt':
            pg = nipyapi.canvas.get_process_group('VTI_data_for_train_moves_process_group', identifier_type='name', greedy=False)
            group_list = self.getGroupProcessorList('VTI_data_for_train_moves_process_group')
        elif service == 'tcm':
            pg = nipyapi.canvas.get_process_group('VTI_data_for_track_health_margin', identifier_type='name', greedy=False)
            group_list = self.getGroupProcessorList('VTI_data_for_track_health_margin')
        else:
            return -1
        # Make sure they're all enabled
        for processor in group_list:
            self.enableProcessor(processor, True)
        # Enable all the ports First
        self.enableInputPorts(pg.id, True)
        self.enableOutputPorts(pg.id, True)
        # Schedule them
        return nipyapi.canvas.schedule_process_group(pg.id, state)

    def scheduleTrunk(self, state):
        processor = self.getProcessor('GetVTIData')
        if processor.component.state == 'DISABLED':
            self.enableProcessor(processor, True)
        self.scheduleProcessor(processor, state)

        # Deal with Extract Features
        pg = nipyapi.canvas.get_process_group('Extract_Features', identifier_type='name', greedy=False)
        group_list = self.getGroupProcessorList('Extract_Features')
        # Enable the ports first (seems to not start all processors if you don't)
        self.enableInputPorts(pg.id, True)
        self.enableOutputPorts(pg.id, True)
        for processor in group_list:
            if processor.component.state == 'DISABLED':
                self.enableProcessor(processor, True)
        nipyapi.canvas.schedule_process_group(pg.id, state)
        # Enable ports within Extract_Features

    def getServiceState(self, service):
        """ Returns the process group status for a given service """
        if service == 'shunt':
            pg = nipyapi.canvas.get_process_group('VTI_data_for_train_moves_process_group', identifier_type='name', greedy=False)
        elif service == 'tcm':
            pg = nipyapi.canvas.get_process_group('VTI_data_for_track_health_margin', identifier_type='name', greedy=False)
        else:
            return -1
        status = nipyapi.canvas.get_process_group_status(pg_id=pg.id, detail='all')
        return status

    def getServiceStatus(self, service):
        """ Boils down the state of a service to True or False """
        state = self.getServiceState(service)
        if state.stopped_count == 0 and state.running_count > 0:
            return True
        else:
            return False
        return state

    def updateGetVTIData(self, processor):
        """ Handles the logic of stopping and starting the shunt and tcm process groups
         when reconfiguring the GetVTIData processor.  Leave them in the state they were in"""
        self.scheduleTrunk(False)
        shunt_status = self.getServiceStatus('shunt')
        tcm_status = self.getServiceStatus('tcm')
        self.scheduleService('shunt', False)
        self.scheduleService('tcm', False)
        status = self.updateProcessor(processor)
        self.scheduleTrunk(True)
        self.scheduleService('shunt', shunt_status)
        self.scheduleService('tcm', tcm_status)
        return status

    def getNifiVersion(self):
        """ Get the nifi version

        """
        return nipyapi.system.get_nifi_version_info()

    def getStatus(self, process_group):
        pg = nipyapi.canvas.get_process_group(process_group, identifier_type='name', greedy=False)
        return nipyapi.canvas.get_process_group_status(pg_id=pg.id, detail='all')


def main():
    print("Look ma, nothing in main.  Good place to test stuff out.")


if __name__ == "__main__":
    main()
